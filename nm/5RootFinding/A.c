#include "header.h"

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n A. Newton's method with back-tracking linesearch and numerical Jacobian\n");
	fprintf(stderr, "_____________________________________________________________________________\n");

	int n = 2;
	double dx = 1e-6, tolerance = 1e-6;
	gsl_vector * iter = gsl_vector_alloc(n); /* steps & func calls */
	gsl_vector * fx= gsl_vector_alloc(n);
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,2);
/*	Can't be solved for initial x=[0,0] as it'll divide by zero?	*/

	fprintf(stderr,"\nExtremum of the System of Equations:\n");

	System_of_Equations(x,fx);	
	vector_print("initial vector x: ",x);
	vector_print("System of Equations(x): ",fx);
	newton(System_of_Equations,x,dx,tolerance, iter);
	vector_print("solution x: ",x);
	System_of_Equations(x,fx);
	vector_print("f(x): ",fx);
	fprintf(stderr, "Steps = %g, function calls = %g\n",
		gsl_vector_get(iter,0), gsl_vector_get(iter,1) );


	fprintf(stderr,"\nExtremum of the Rosenbrock's function:\n");
	gsl_vector_set_zero(x);
	Rosenbrock(x,fx);	
	vector_print("initial vector x: ",x);
	vector_print("Rosenbrock(x): ",fx);
	newton(Rosenbrock,x,dx,tolerance, iter);
	vector_print("solution x: ",x);
	Rosenbrock(x,fx);
	vector_print("f(x): ",fx);	
	fprintf(stderr, "Steps = %g, function calls = %g\n",
		gsl_vector_get(iter,0), gsl_vector_get(iter,1) );

	fprintf(stderr,"\nExtremum of the Himmelblau's function:\n");
	gsl_vector_set_zero(x);
	Himmelblau(x,fx);	
	vector_print("initial vector x: ",x);
	vector_print("Himmelblau(x): ",fx);
	newton(Himmelblau,x,dx,tolerance, iter);
	vector_print("solution x: ",x);
	Himmelblau(x,fx);
	vector_print("f(x): ",fx);	
	fprintf(stderr, "Steps = %g, function calls = %g\n",
		gsl_vector_get(iter,0), gsl_vector_get(iter,1) );


	return 	EXIT_SUCCESS;
}