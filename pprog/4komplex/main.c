#include "komplex.h"
#include "stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};
	double x; double y;

	printf("\ntesting komplex_new...\n"); /* returns x+i*y */
	x = 1.1;	y = 2.1;
	komplex z = komplex_new(x,y);
	komplex z_test_new = {1.1, 2.1};
	komplex_print("z =", z);	
	komplex_print("z should now be =", z_test_new);
	komplex_equal(z_test_new,z,TINY,TINY);


	printf("\ntesting komplex_set\n"); /* z is set to x+i*y */
	x = 1.2;	y=2.2;
	komplex_set(&z, x,y);
	komplex z_test_set = {1.2, 2.2};
	komplex_print("z is now =",z);
	komplex_print("z should now be =", z_test_set);
	komplex_equal(z_test_set,z,TINY,TINY);
	

	printf("\ntesting komplex_add...\n"); /* returns a+b */
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);
	komplex_equal(R,r,TINY,TINY);

	printf("\ntesting komplex_mult\n");
	komplex m = komplex_mul(a,b);
	komplex_print("a*b =", m);

/* the following is optional */


}