#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

//	allocate dynamic memory
	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

// set and get array value
	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	printf("vi = %g, value = %g\n",vi, value );
	if (double_equal(vi, value)) printf("test passed\n");
	else printf("test failed\n");

//  add arrays
	printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_add(a, b); // a_i ← a_i + b_i
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);
	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	// dot product
	printf("\nmain: testing nvector_dot_product\n");
	double d;
	d = nvector_dot_product (a, b);   /* returns dot-product */
	printf("dot product = %g\n", d);

// 	Set zero
	printf("\nmain: testing nvector_set_zero\n");
	nvector *e = nvector_alloc(n);
	nvector_set_zero(e);             /* all elements ← 0 */
	nvector_print("vector e = ",e);

//	Free all dynamic arrays, or else...!
	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);
	nvector_free(e);
	return 0;
}