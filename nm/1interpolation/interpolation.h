#ifndef HAVE_INTERPOLATION_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* Functions	*/

int binary_search(int dim, double * x, double z);

/* Linear Interpolation 	*/
double linear_interpolation(int n, double *x, double *y, double z);
double linear_interpolation_integral(
	int dim, double *x, double *y, double z);

/*	Quadratic Interpolation 	*/
typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline * qspline_alloc(int n); /* allocates and builds the quadratic spline */
qspline * qspline_coeff(qspline *s, double *x, double *y);	/*	Creates b_i and c_i	*/
double qspline_evaluate(qspline *s, double z);        /* evaluates the prebuilt spline at point z */
double qspline_derivative(qspline *s, double z); /* evaluates the derivative of the prebuilt spline at point z */
double qspline_integral(qspline *s, double z);  /* evaluates the integral of the prebuilt spline from x[0] to z */
void qspline_free(qspline *s); /* free memory allocated in qspline_alloc */

/* Cubic Interpolation 	*/
typedef struct {int n; double *x, *y, *b, *c, *d;} cubic_spline;
cubic_spline * cubic_spline_alloc(int n); /* allocates and builds the quadratic spline */
cubic_spline * cubic_spline_coeff(cubic_spline *s, double *x, double *y);	/*	Creates b_i and c_i	*/
double cubic_spline_evaluate(cubic_spline *s, double z);        /* evaluates the prebuilt spline at point z */
double cubic_spline_derivative(cubic_spline *s, double z); /* evaluates the derivative of the prebuilt spline at point z */
double cubic_spline_integral(cubic_spline *s, double z);  /* evaluates the integral of the prebuilt spline from x[0] to z */
void cubic_spline_free(cubic_spline *s); /* free memory allocated in cubic_spline_alloc */

#define HAVE_INTERPOLATION_H
#endif