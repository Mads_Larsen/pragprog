#include "exam.h"

int main()
{	
	fprintf(stderr, "\n________________________________________________________\n");
	fprintf(stderr, "\n Practical Programming & Numerical Methods Exam Project\n");
	fprintf(stderr, " Mads Julsgaard Aagaard Larsen 201406907\n");
	fprintf(stderr, " ODE: A Two-Step Method\n");
	fprintf(stderr, "________________________________________________________\n");
	
	fprintf(stderr, "\nThe two-step method is tested normally and with extra evaluations.\n");
	fprintf(stderr, "\nThe first step is estimated using the Runga-Kutta 12 method within the adaptive driver.\n");
	
	fprintf(stderr, "\n A harmonic Oscillator system is solved using the two methods:\n");
	fprintf(stderr, "y''=-y for y(0)=1 and y'(0)=0, So y' = Cosine with indefinite integral y = -Sine\n");
	int n = 2;
	double a = 0, b = M_PI/2, h, acc = 1e-6, eps = 1e-4;
	gsl_vector*y = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);

	fprintf(stderr, "\nUsing accuracy %g and relative accuracy %g\n", acc,eps );

/*	Two-Step Method 	*/
	gsl_vector_set(y,0,1);	/*	y(0)=1	*/
	gsl_vector_set(y,1,0);	/*	y'(0)=0	*/
	h = copysign(0.01,b-a);	/*	step direction dependent on sign	*/

	int iter = adaptive_driver(&a,b,&h,y,acc,eps,&two_step,harm_osci, err);

	fprintf(stderr, "\nTwo-Step Method. Iterations = %i\n",iter);
	fprintf(stderr, "Function\tSolved\t\tExact\t\tSolved Error\tExact error\n");
	fprintf(stderr, "y' cos(pi/2)\t%4g\t%1f\t%4g\t%4g\n",
		gsl_vector_get(y,0), cos(a), gsl_vector_get(err,0), gsl_vector_get(y,0)-cos(a));
	fprintf(stderr, "y -sin(pi/2)\t%f\t%4g\t\t%4g\t%4g\n\n",
		gsl_vector_get(y,1), -sin(a), gsl_vector_get(err,1), gsl_vector_get(y,1)+sin(a));

/*	Two-Step Method with Extra evaluations 	*/
	a = 0;
	gsl_vector_set(y,0,1);	/*	y(0)=1	*/
	gsl_vector_set(y,1,0);	/*	y'(0)=0	*/
	h = copysign(0.01,b-a);	/*	step direction dependent on sign	*/

	iter = adaptive_driver(&a,b,&h,y,acc,eps,&two_step_extra,harm_osci, err);

	fprintf(stderr, "\nTwo-Step Method with Extra Evaluations. Iterations = %i\n",iter);
	fprintf(stderr, "Function\tSolved\t\tExact\t\tSolved Error\tExact error\n");
	fprintf(stderr, "y' cos(pi/2)\t%4g\t%1f\t%4g\t%4g\n",
		gsl_vector_get(y,0), cos(a), gsl_vector_get(err,0), gsl_vector_get(y,0)-cos(a));
	fprintf(stderr, "y -sin(pi/2)\t%f\t%4g\t\t%4g\t%4g\n\n",
		gsl_vector_get(y,1), -sin(a), gsl_vector_get(err,1), gsl_vector_get(y,1)+sin(a));

/*	ODE Path 	*/

	fprintf(stderr, "\nA modified version of the two-step adaptive driver stores the path, see adaptive_driver.c.\n");
	fprintf(stderr, "It calculates and stores the path of a relativistic precession of a planetary orbit\n");
	fprintf(stderr, "The orbit can be seen on Fig. orbit.svg\n");

	int max_steps = 1e6;
	n = 2;
	a = 0; b = 20*M_PI; acc = 1e-5; eps = 1e-5;
	gsl_matrix* path = gsl_matrix_calloc(max_steps, n+1); /* [t y1...yn]	*/
	
	gsl_matrix_set(path,0,0, a);	/*	initial t 	*/
	gsl_matrix_set(path,0,1, 1);	/* initial y0	*/
	gsl_matrix_set(path,0,2, -0.5);	/* initial y1	*/
	h = copysign(0.01,b-a);	/*	step direction dependent on sign	*/
    
	int steps = adaptive_driver_path(path,b,&h,acc,eps,&two_step_extra,orbit);

	for(int i = 0; i<steps; i++){
		fprintf(stdout, "%g\t%g\t%g\n",
			gsl_matrix_get(path,i,0), gsl_matrix_get(path,i,1), gsl_matrix_get(path,i,2));
	}

/*	Free Allocated Memory	*/
	gsl_vector_free(y); gsl_vector_free(err);
	gsl_matrix_free(path);

	return EXIT_SUCCESS;
}