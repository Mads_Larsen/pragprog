#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <math.h>

/* Without analytical Jacobian Matrix */
void System_of_Equations(gsl_vector* p, gsl_vector* fx){
/*	A*x*y = 1 ,
	exp(-x) + exp(-y) = 1 + 1/A,	*/
	int A = 10000;
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
	gsl_vector_set(fx,0, A*x*y-1);					/*	Eq 1 = 0	*/
	gsl_vector_set(fx,1, exp(-x)+exp(-y)-1.-1.0/A);	/*	Eq 2 = 0	*/
	}

void Rosenbrock(gsl_vector* p,gsl_vector* fx){
/*	f(x,y) = (1-x)^2+100(y-x^2)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
	gsl_vector_set(fx,0, -2*(1-x)+400*(y-x*x)*(-1)*x);	/* dfdx */
	gsl_vector_set(fx,1, 200*(y-x*x)); 					/* dfdy */
	}

void Himmelblau(gsl_vector* p,gsl_vector* fx){
/*	f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
	gsl_vector_set(fx,0, 4*x*(x*x+y-11)+2*(x+y*y-7)); /* dfdx */
	gsl_vector_set(fx,1, 2*(x*x+y-11)+4*y*(x+y*y-7)); /* dfdy */
	}

/* With analytical Jacobian matrix */
void System_of_Equations_Jacobian(gsl_vector* p, gsl_vector* fx, gsl_matrix* J){
/*	A*x*y = 1 ,
	exp(-x) + exp(-y) = 1 + 1/A,	*/
	int A = 10000;
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	fx	*/
	gsl_vector_set(fx,0, A*x*y-1);					/*	Eq 1 = 0	*/
	gsl_vector_set(fx,1, exp(-x)+exp(-y)-1.-1.0/A);	/*	Eq 2 = 0	*/ 
/*	Analytical Jacobian Matrix 	*/
	gsl_matrix_set(J,0,0,	A*y);
	gsl_matrix_set(J,0,1,	A*x);
	gsl_matrix_set(J,1,0,	-exp(-x));
	gsl_matrix_set(J,1,1,	-exp(-y));
	}

void Rosenbrock_Jacobian(gsl_vector* p, gsl_vector* fx, gsl_matrix* J){
/*	f(x,y) = (1-x)^2+100(y-x^2)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	fx	*/
	gsl_vector_set(fx,0, -2*(1-x)+400*(y-x*x)*(-1)*x); 	/* dfdx */
	gsl_vector_set(fx,1, 200*(y-x*x)); 					/* dfdy */
/*	Analytical Jacobian Matrix 	*/
	gsl_matrix_set(J,0,0,	2-400*(y-3*x*x));
	gsl_matrix_set(J,0,1,	-400*x);
	gsl_matrix_set(J,1,0,	-400*x);
	gsl_matrix_set(J,1,1,	200);
	}

void Himmelblau_Jacobian(gsl_vector* p, gsl_vector* fx, gsl_matrix* J){
/*	f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	fx	*/
	gsl_vector_set(fx,0, 4*x*(x*x+y-11)+2*(x+y*y-7)); /* dfdx */
	gsl_vector_set(fx,1, 2*(x*x+y-11)+4*y*(x+y*y-7)); /* dfdy */
/*	Analytical Jacobian Matrix 	*/
	gsl_matrix_set(J,0,0,	4*(3*x*x+y-11)+2);
	gsl_matrix_set(J,0,1,	4*x+4*y);
	gsl_matrix_set(J,1,0,	4*x+4*y);
	gsl_matrix_set(J,1,1,	2+4*(x+3*y*y-7));
	}

/* For gsl root With analytical Jacobian matrix */
int System_of_Equations_Jacobian_gsl(const gsl_vector* p, void *params, gsl_vector* fx, gsl_matrix* J){
/*	A*x*y = 1 ,
	exp(-x) + exp(-y) = 1 + 1/A,	*/
	int A = 10000;
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	fx	*/
	gsl_vector_set(fx,0, A*x*y-1);					/*	Eq 1 = 0	*/
	gsl_vector_set(fx,1, exp(-x)+exp(-y)-1.-1.0/A);	/*	Eq 2 = 0	*/ 
/*	Analytical Jacobian Matrix 	*/
	gsl_matrix_set(J,0,0,	A*y);
	gsl_matrix_set(J,0,1,	A*x);
	gsl_matrix_set(J,1,0,	-exp(-x));
	gsl_matrix_set(J,1,1,	-exp(-y));
	return GSL_SUCCESS;

	}

int Rosenbrock_Jacobian_gsl(const gsl_vector* p, void *params, gsl_vector* fx, gsl_matrix* J){
/*	f(x,y) = (1-x)^2+100(y-x^2)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	fx	*/
	gsl_vector_set(fx,0, -2*(1-x)+400*(y-x*x)*(-1)*x); 	/* dfdx */
	gsl_vector_set(fx,1, 200*(y-x*x)); 					/* dfdy */
/*	Analytical Jacobian Matrix 	*/
	gsl_matrix_set(J,0,0,	2-400*(y-3*x*x));
	gsl_matrix_set(J,0,1,	-400*x);
	gsl_matrix_set(J,1,0,	-400*x);
	gsl_matrix_set(J,1,1,	200);
	return GSL_SUCCESS;
	}

int Himmelblau_Jacobian_gsl(const gsl_vector* p, void *params, gsl_vector* fx, gsl_matrix* J){
/*	f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	fx	*/
	gsl_vector_set(fx,0, 4*x*(x*x+y-11)+2*(x+y*y-7)); /* dfdx */
	gsl_vector_set(fx,1, 2*(x*x+y-11)+4*y*(x+y*y-7)); /* dfdy */
/*	Analytical Jacobian Matrix 	*/
	gsl_matrix_set(J,0,0,	4*(3*x*x+y-11)+2);
	gsl_matrix_set(J,0,1,	4*x+4*y);
	gsl_matrix_set(J,1,0,	4*x+4*y);
	gsl_matrix_set(J,1,1,	2+4*(x+3*y*y-7));
	return GSL_SUCCESS;

	}