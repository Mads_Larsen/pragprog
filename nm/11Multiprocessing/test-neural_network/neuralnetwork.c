#include "neural.h"
#define DIFF(x) pow(x,2)

/* 2D	*/


/*	n: #neurons, f: activation function	*/
neuron* neuron_alloc_2D(int n,double(*f)(double)){
	neuron* network = malloc(sizeof(neuron));
	network->n=n;
	network->f=f;
	network->data=gsl_vector_alloc(5*n);	/*	a,b,w for every n	*/
	return network;
}

void neuron_free(neuron* network){
	gsl_vector_free(network->data);
	free(network);
}

double neuron_feed_forward_2D(neuron* network,double x, double y){
	double output_sum1=0, output_sum2=0;
	#pragma omp parallel sections
	{
		#pragma omp section
		{
			for(int i=0; i < (network->n)*1/2; i++){
			/*	5*i to skip to next neuron 	*/
				double a=gsl_vector_get(network->data,5*i+0);
				double b=gsl_vector_get(network->data,5*i+1);
				double c=gsl_vector_get(network->data,5*i+2);
				double d=gsl_vector_get(network->data,5*i+3);
				double w=gsl_vector_get(network->data,5*i+4); /* Weight */

				double xarg = (x+a)/b;
				double yarg = (y+c)/d;
			/*	Product activation function	f((x+a)/b) * f((y+c)/d)
				They have to make a full basis	*/
				output_sum1 += network->f(xarg) * network->f(yarg) * w;
			}
		}
		#pragma omp section
		{
			for(int i=(network->n)/2; i < (network->n)*2/2; i++){
			/*	5*i to skip to next neuron 	*/
				double a=gsl_vector_get(network->data,5*i+0);
				double b=gsl_vector_get(network->data,5*i+1);
				double c=gsl_vector_get(network->data,5*i+2);
				double d=gsl_vector_get(network->data,5*i+3);
				double w=gsl_vector_get(network->data,5*i+4); /* Weight */

				double xarg = (x+a)/b;
				double yarg = (y+c)/d;
			/*	Product activation function	f((x+a)/b) * f((y+c)/d)
				They have to make a full basis	*/
				output_sum2 += network->f(xarg) * network->f(yarg) * w;
			}
		}
	}
	return output_sum1+output_sum2;
}

void neuron_train_2D(neuron* network,gsl_matrix* params_train,gsl_vector* f_train){
	
	int number_neurons = params_train->size1;
	
	double delta(gsl_vector* p){
		gsl_vector_memcpy(network->data,p);
		double deviation_sum1=0, deviation_sum2=0, deviation_sum3=0;
	/*	The following sections will run parallel in separate threads */
	#pragma omp parallel sections 
	{
		#pragma omp section
		{
			for(int i=0; i<number_neurons *1/3; i++){
				double x=gsl_matrix_get(params_train,i, 0);
				double y=gsl_matrix_get(params_train,i, 1);
				double f=gsl_vector_get(f_train,i);
				double F=neuron_feed_forward_2D(network, x,y);
				deviation_sum1 += DIFF(F-f);	/* Fp(xk) - fk  (²?)*/
			}
		}
		#pragma omp section
		{
			for(int i = number_neurons*1/3; i < number_neurons*2/3; i++){
				double x=gsl_matrix_get(params_train,i, 0);
				double y=gsl_matrix_get(params_train,i, 1);
				double f=gsl_vector_get(f_train,i);
				double F=neuron_feed_forward_2D(network, x,y);
				deviation_sum2 += DIFF(F-f);	/* Fp(xk) - fk  (²?)*/
			}
		}
		#pragma omp section 
		{
			for(int i= number_neurons*2/3; i < number_neurons*3/3; i++){
				double x=gsl_matrix_get(params_train,i, 0);
				double y=gsl_matrix_get(params_train,i, 1);
				double f=gsl_vector_get(f_train,i);
				double F=neuron_feed_forward_2D(network, x,y);
				deviation_sum3 += DIFF(F-f);	/* Fp(xk) - fk  (²?)*/
			}
		}
	}
		return deviation_sum1 + deviation_sum2 + deviation_sum3;
	}
	gsl_vector* p=gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
/*	Find parameters minimizing the deviation */
	quasi_newton(delta,p,1e-3);
	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);
}
