#include "interpolation.h"

/*	Binary Search of the interval [xi ≤ x ≤ xi+1] */
int binary_search(int dim, double * x, double z){
	int i = 0, j = dim-1, m;

	while (j-i > 1){
		m = (i+j)/2;
		if (z>x[m]){
			i = m;
		}
		else j = m;
	}

	return i;
}