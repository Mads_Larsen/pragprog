#include "lineq.h"

int main(){
	fprintf(stderr, "\n B. Matrix inverse by Gram-Schmidt QR factorization\n");

/*	Define square matrix */
	int m = 3;
	gsl_matrix * A = gsl_matrix_alloc(m,m);
	gsl_matrix * A_copy = gsl_matrix_alloc(m,m); /* To be destroyed */
	gsl_matrix * B = gsl_matrix_alloc(m,m);
	gsl_matrix * Q = gsl_matrix_alloc(m,m);
	gsl_matrix * R = gsl_matrix_alloc(m,m);
	gsl_matrix * AB = gsl_matrix_alloc(m,m);

/*	generate random matrix A and copy 	*/
	matrix_fill(A);
	gsl_matrix_memcpy(A_copy, A);
	matrix_print("Square Matrix A\n", A);

/*	Factorize A to QR	*/
	gs_decomp(A_copy,Q,R);
	matrix_print("Q matrix", Q);
	matrix_print("R matrix", R);

/*	Invert A to B=A⁻1	*/
	gs_inverse(Q,R,B);
	matrix_print("Inver matrix B = A^-1", B);

/*	Check that product is identity	*/
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, A, B, 0, AB);
	matrix_print("AB = Should be I", AB);

/* 	Free allocated memory	*/
	gsl_matrix_free(A); gsl_matrix_free(A_copy);
	gsl_matrix_free(B);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);

	return EXIT_SUCCESS;
}