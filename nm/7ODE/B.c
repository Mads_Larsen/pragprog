#include "header.h"

void harm_osci(double x, gsl_vector * y, gsl_vector * dydx){
/*	Harmonic osc.. u''=-u -> y1=u, y2=u' -> y1'=y2, y2'=-y1	*/
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
}

int main(){
	fprintf(stderr, "\n____________________________________________________\n");
	fprintf(stderr, "\n B. Storing the path\n");
	fprintf(stderr, "____________________________________________________\n");

	int n = 2, max_steps = 1e3;
	double a = 0, b = M_PI/2, h, abs = 1e-3, eps = 1e-3;
	gsl_matrix* path = gsl_matrix_calloc(max_steps, n+1); /* [t y1...yn]	*/
	
	gsl_matrix_set(path,0,0, a);	/*	initial t 	*/
	gsl_matrix_set(path,0,1, 1);	/*	y(0)=1	*/
	gsl_matrix_set(path,0,2, 0);	/*	y'(0)=0	*/
	h = copysign(0.01,b-a);	/*	step direction dependent on sign	*/
    
	int steps = driver_path(path,b,&h,abs,eps,&rkstep12,harm_osci);
	fprintf(stderr, "steps = %i\n", steps);

/*	Plot path 	*/
	fprintf(stdout, "x\t y\t dydt\n");
	for(int i = 0; i<steps; i++){
		fprintf(stdout, "%g\t %g\t %g\n",
			gsl_matrix_get(path,i,0),
			gsl_matrix_get(path,i,1),
			gsl_matrix_get(path,i,2)
		);
	}
	fprintf(stdout, "\n\n");

	fprintf(stderr,"\nSolve y'' = -y for y(0) = 1 and y'(0)=0\n");
	fprintf(stderr,"Solved solution is\t y'(%g) = %g\ty(%g) = %g\n",
		gsl_matrix_get(path,steps,0), gsl_matrix_get(path,steps,1),
		gsl_matrix_get(path,steps,0), gsl_matrix_get(path,steps,2));
	fprintf(stderr,"Exact solution is\t cos(%g) = %g\t-sin(%g) = %g\n",
		gsl_matrix_get(path,steps,0), cos(gsl_matrix_get(path,steps,0)),
		gsl_matrix_get(path,steps,0), -sin(gsl_matrix_get(path,steps,0)));


	return EXIT_SUCCESS;
}