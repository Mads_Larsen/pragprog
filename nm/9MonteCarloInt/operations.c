#include "header.h"
#define RND ((double)rand()/RAND_MAX)

void randomx(int dim, gsl_vector*a, gsl_vector*b, gsl_vector*x){
	for(int i=0; i<dim; i++){
//		x[i] = a[i] + RND*(b[i]-a[i]);
		gsl_vector_set(x,i,
			gsl_vector_get(a,i) +
			RND*( gsl_vector_get(b,i) - gsl_vector_get(a,i) ) );
	}
}

void MonteCarlo_plain(
	double f(gsl_vector*x), 
	gsl_vector*a, gsl_vector*b, 
	int N, int dim,
	double*result, double*error){

	double volume = 1;
	for(int i=0; i<dim; i++){
//		volume*=b[i]-a[i];
		volume *= gsl_vector_get(b,i)-gsl_vector_get(a,i);
	}

	double sum=0, sum2=0, fx;
	gsl_vector *x = gsl_vector_alloc(dim);

	for (int i=0; i<N; i++){
		randomx(dim,a,b,x);
		fx = f(x);
		sum+=fx;
		sum2+=pow(fx,2);
	}

	double average = sum/N;
	double variance = sum2/N - pow(average,2);

	*result = average*volume;
	*error = sqrt(variance/N)*volume;

	gsl_vector_free(x);

}