#ifndef HAVE_NEURAL_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
#include <omp.h>

/*	Functions	*/
typedef struct {int n; double (*f)(double); gsl_vector* data;} neuron;

/*	Operations	*/

/*	2D	*/
void neuron_free(neuron* network);
neuron* neuron_alloc_2D(int n, double(*f)(double));
double neuron_feed_forward_2D(neuron* network, double x, double y);
void neuron_train_2D(neuron* network,
	gsl_matrix* params_train, gsl_vector* f_train);

/*	Quasi Newton Minimization 	*/
void numeric_gradient(double f(gsl_vector*), gsl_vector*x, gsl_vector*grad);
int quasi_newton(
double f(gsl_vector* p),/* f: objective function, gradient: gradient, H: Hessian matrix H*/
gsl_vector* x, /* starting point, becomes the latest approximation to the root on exit */
double accuracy); /* accuracy goal, on exit |gradient|<eps */

/* 	Tools	*/
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void vector_print(const char* s,const gsl_vector* v);	// Prints vector

#define HAVE_NEURAL_H
#endif