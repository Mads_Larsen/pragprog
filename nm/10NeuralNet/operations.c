#include "header.h"
#define DIFF(x) pow(x,2)

/* 1D	*/

/*	n: #neurons, f: activation function	*/
neuron* neuron_alloc(int n,double(*f)(double)){
	neuron* network = malloc(sizeof(neuron));
	network->n=n;
	network->f=f;
	network->data=gsl_vector_alloc(3*n);	/*	a,b,w for every n	*/
	return network;
}
void neuron_free(neuron* network){
	gsl_vector_free(network->data);
	free(network);
}

double neuron_feed_forward(neuron* network,double x){
	double output_sum=0;
	for(int i=0; i<network->n; i++){
	/*	3*i to skip to next neuron 	*/
		double a=gsl_vector_get(network->data,3*i+0);
		double b=gsl_vector_get(network->data,3*i+1);
		double w=gsl_vector_get(network->data,3*i+2); /* Weight */
		output_sum += network->f((x+a)/b) * w;
	}
	return output_sum;
}

void neuron_train(neuron* network,gsl_vector* x_train,gsl_vector* f_train){
	double delta(gsl_vector* p){
		gsl_vector_memcpy(network->data,p);
		double deviation_sum=0;
		for(int i=0; i<x_train->size; i++){
			double x=gsl_vector_get(x_train,i);
			double f=gsl_vector_get(f_train,i);
			double y=neuron_feed_forward(network,x);
			deviation_sum+=DIFF(y-f);	/* Fp(xk) - fk  (²?)*/
		}
		/*	Normalize output */
		return deviation_sum;
	}
	gsl_vector* p=gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
/*	Find paramseters minimizing the deviation */
	quasi_newton(delta,p,1e-3);
	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);
}





/* 2D	*/

/*	n: #neurons, f: activation function	*/
neuron* neuron_alloc_2D(int n,double(*f)(double)){
	neuron* network = malloc(sizeof(neuron));
	network->n=n;
	network->f=f;
	network->data=gsl_vector_alloc(5*n);	/*	a,b,w for every n	*/
	return network;
}


double neuron_feed_forward_2D(neuron* network,double x, double y){
	double output_sum=0;
	for(int i=0;i<network->n;i++){
	/*	5*i to skip to next neuron 	*/
		double a=gsl_vector_get(network->data,5*i+0);
		double b=gsl_vector_get(network->data,5*i+1);
		double c=gsl_vector_get(network->data,5*i+2);
		double d=gsl_vector_get(network->data,5*i+3);
		double w=gsl_vector_get(network->data,5*i+4); /* Weight */

		double xarg = (x+a)/b;
		double yarg = (y+c)/d;
	/*	Product activation function	f((x+a)/b) * f((y+c)/d)
		They have to make a full basis	*/
		output_sum += network->f(xarg) * network->f(yarg) * w;
	}
	return output_sum;
}

void neuron_train_2D(neuron* network,gsl_matrix* params_train,gsl_vector* f_train){
	int number_neurons = params_train->size1;
	double delta(gsl_vector* p){
		gsl_vector_memcpy(network->data,p);
		double deviation_sum=0;
		for(int i=0; i<number_neurons; i++){
			double x=gsl_matrix_get(params_train,i, 0);
			double y=gsl_matrix_get(params_train,i, 1);

			double f=gsl_vector_get(f_train,i);
			double F=neuron_feed_forward_2D(network, x,y);

			deviation_sum += DIFF(F-f);	/* Fp(xk) - fk  (²?)*/
		}
		/*	Normalize output */
		return deviation_sum;
	}
	gsl_vector* p=gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
/*	Find parameters minimizing the deviation */
	quasi_newton(delta,p,1e-3);
	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);
}
