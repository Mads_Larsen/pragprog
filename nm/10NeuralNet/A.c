#include "header.h"

double activation_function(double x){return x*exp(-x*x);}
double function_to_fit(double x){return cos(5*x)*exp(-x*x);}

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n A. Artificial Neural trained to interpolate functions\n");
	fprintf(stderr, "_____________________________________________________________________________\n");

	
	double x_min=-1,x_max=1;
	int training_points=20;
	gsl_vector* x_train=gsl_vector_alloc(training_points);
	gsl_vector* f_train=gsl_vector_alloc(training_points);

/*	Set training values	*/
	for(int i=0; i<training_points; i++){
		double normalize = i/(training_points-1.0);
		double x=x_min+(x_max-x_min)*normalize;
		double f=function_to_fit(x);
		gsl_vector_set(x_train,i,x);
		gsl_vector_set(f_train,i,f);
	}

/*	Neural network	*/
	int number_neurons=10;
	neuron* network=neuron_alloc(number_neurons,activation_function);

/*	Set initial guess for parameters	*/
	for(int i=0; i<network->n; i++){
		double normalize = i/(network->n-1);
		gsl_vector_set(network->data,3*i+0,x_min+(x_max-x_min)*normalize); /*a*/
		gsl_vector_set(network->data,3*i+1,1);	/*b*/
		gsl_vector_set(network->data,3*i+2,1);	/*weight*/
	}
/*	Find parameters minimizing the deviation, using training data */
	neuron_train(network,x_train,f_train);

/*	Print training values	*/
	for(int i=0; i<x_train->size; i++){
		double x=gsl_vector_get(x_train,i);
		double f=gsl_vector_get(f_train,i);
		fprintf(stdout,"%g %g\n",x,f);
	}
	fprintf(stdout,"\n\n");

/*	Generate output values using trained parameters, and print them	*/
	double dz=1.0/64;
	for(double z=x_min; z<=x_max; z+=dz){
		double output=neuron_feed_forward(network,z);
		fprintf(stdout,"%g %g\n",z,output);
	}
	fprintf(stdout, "\n\n");

/*	Print to log	*/
	fprintf(stderr, "\n");
	fprintf(stderr, "Number of training points: %i\n", training_points );	
	fprintf(stderr, "Number of Neurons: %i\n", number_neurons );
	fprintf(stderr, "Fitting function cos(5*x)*exp(-x*x)\n");
	fprintf(stderr, "See trained fit in Fig. interpolation.svg\n");

	neuron_free(network);
	gsl_vector_free(x_train); gsl_vector_free(f_train);

	return EXIT_SUCCESS;
}