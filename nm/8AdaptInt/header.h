#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/*	Functions	*/												
								/* int_0^1 fx */
//double func_sqrt 	(double x);	/* = 2/3	*/
//double func_invsqrt	(double x);	/* = 2		*/
//double func_logsqrt	(double x);	/* =-4		*/
//double func_pi		(double x);	/* = pi 	*/
double func(double f(double), double x, double a, double b);
double func_Clenshaw_Curtis(double f(double), double theta, double a, double b);

/*	Operations	*/
double integrator_recursion(
	double f (double) ,
	double a , double b , 
	double acc , double eps,
	double f2 , double f3 ,
	double *err, int recursions);

double integrator(
	double f(double), 
	double a, double b,
	double acc, double eps, 
	double *err);

double integrator_limits(
	double f(double), 
	double a, double b,
	double acc, double eps, 
	double *err);
double integrator_Clenshaw_Curtis(
	double f(double x),
	double a, double b, 
	double acc, double eps, 
	double *err);
double integrator_recursion_Clenshaw_Curtis(
	double f (double) ,
	double a , double b , 
	double acc , double eps,
	double f2 , double f3 ,
	double *err, int recursions);

/* 	Tools	*/
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void vector_print(const char* s,const gsl_vector* v);	// Prints vector

#define HAVE_HEADER_H
#endif