#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <assert.h>

/*	Find the root of x-cos(a)	*/
int cosine_root
(const gsl_vector* variable, void* params, gsl_vector* f){
	double x=*(double*)params;
	double a=gsl_vector_get(variable,0);
	gsl_vector_set(f,0,x-cos(a));
return GSL_SUCCESS;
}

double arccos(double x){
/* arccosine only defined in -1 to 1	*/
	if (x <-1 || x>1){
		return NAN;
	}

	int dim = 1;

/*	Initialize gsl_multiroot_function*/
	gsl_multiroot_function func;
	func.f=cosine_root;
	func.n=dim;
	func.params=(void*)&x;

/*	Initialize solver*/
	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver* solver = gsl_multiroot_fsolver_alloc(T,dim);

	gsl_vector* guess = gsl_vector_alloc(dim);
	gsl_vector_set(guess,0,0.5*M_PI);
	gsl_multiroot_fsolver_set(solver,&func,guess);

/*	Iterate 	*/
	int status, iter=0;
	double acc = 1e-3;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(solver);
		status = gsl_multiroot_test_residual(solver->f,acc);
	}while(status==GSL_CONTINUE && iter<1000);

/*	Print result	*/
	double result = gsl_vector_get(solver->x,0);

/*	Free allocated memory	*/
	gsl_vector_free(guess);
	gsl_multiroot_fsolver_free(solver);
	return result;
}