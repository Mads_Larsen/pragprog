#include "header.h"


int main(){
	fprintf(stderr, "\n____________________________________________________\n");
	fprintf(stderr, "\n A. Recursive Adaptive Integrator\n");
	fprintf(stderr, "____________________________________________________\n");

	double Q;
	int evals=0;
	double a=0, b=1, err, acc=0.000001, eps=0.000001;

	fprintf(stderr, "integrand\t calculated\t exact\t evals\t error\n");

	double func_sqrt 	(double x){return sqrt(x);}				/* = 2/3	*/
	Q = integrator(func_sqrt, a, b, acc, eps, &err);
	fprintf(stderr, "√(x)\t\t %g\t 2/3\t %i\t %g\n", Q, evals, err);

//	evals = 0;
	double func_invsqrt	(double x){return pow(sqrt(x),-1);}			/* = 2		*/
	Q = integrator(func_invsqrt, a, b, acc, eps, &err);
	fprintf(stderr, "1/√(x)\t\t %g\t\t 2\t %i\t %g\n", Q, evals, err);
	
//	evals=0;
	double func_logsqrt	(double x){return log(x)*pow(sqrt(x),-1);}	/* =-4		*/
	Q = integrator(func_logsqrt, a, b, acc, eps, &err);
	fprintf(stderr, "ln(x)/√(x)\t %g\t\t -4\t %i\t %g\n", Q, evals, err);

//	evals=0;
	double func_pi	(double x){return 4*sqrt(1-pow(1-x,2));}	/* = pi 	*/
	Q = integrator(func_pi, a, b, acc, eps, &err);
	fprintf(stderr, "4*√(1-(1-x)²)\t %g\t pi\t %i\t %g\n", Q, evals, err);

	return EXIT_SUCCESS;
}

