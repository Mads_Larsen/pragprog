#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
#include <time.h>

/*	Functions	*/												
double fun_square(gsl_vector*x);
double fun_spherical(gsl_vector*x);
double fun_cossine(double x, double y);
/* adaptive integral */
double func(double f(double), double x, double a, double b);
double func_Clenshaw_Curtis(double f(double), double theta, double a, double b);

/*	Operations	*/
void randomx(int dim, gsl_vector*a, gsl_vector*b, gsl_vector*x);
void MonteCarlo_plain(
	double f(gsl_vector*x),
	gsl_vector*a, gsl_vector*b, 
	int N, int dim,
	double*result, double*error);

/* adaptive	integral*/
double integrator_2D(
	double f(double x, double y),
	double c(double), double d(double),
	double a, double b, 
	double acc, double eps,
	double *err);
double outer_integral(double x);
double inner_integral(double y);

double integrator_recursion(
	double f (double) ,
	double a , double b , 
	double acc , double eps,
	double f2 , double f3 ,
	double *err, int recursions);

double integrator(
	double f(double), 
	double a, double b,
	double acc, double eps, 
	double *err);

double integrator_limits(
	double f(double), 
	double a, double b,
	double acc, double eps, 
	double *err);
double integrator_Clenshaw_Curtis(
	double f(double x),
	double a, double b, 
	double acc, double eps, 
	double *err);
double integrator_recursion_Clenshaw_Curtis(
	double f (double) ,
	double a , double b , 
	double acc , double eps,
	double f2 , double f3 ,
	double *err, int recursions);
/* 	Tools	*/
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void vector_print(const char* s,const gsl_vector* v);	// Prints vector

#define HAVE_HEADER_H
#endif