#include<stdio.h>
#include <stdlib.h>
#include<math.h>
double arccos(double x);

int main(){

/*	x values	*/
	double x, xmin = -2,	xmax = 2, dx = 1e-2;

/*	Prints solved a = arccos(x)	*/
	printf("x\ta\n");
	for(x=xmin ; x<= xmax ; x+=dx )
		printf("%3g\t%.5g\n",x,arccos(x));

	printf("\n\n");

/*	prints math.h arccos for reference	*/
	printf("x\ta\n");
	for(x=-1 ; x<= 1 ; x+=1e-1 )
		printf("%.1g\t%.5g\n",x,acos(x));

return EXIT_SUCCESS;
}