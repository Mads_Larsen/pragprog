#include "header.h"
double fun_square(gsl_vector*x){
	double result = 0;
	for(int i=0; i< x->size; i++){
		result += pow(gsl_vector_get(x,i),2); /*	x²+y²+z²+...	*/
	}
	return result;
}

double fun_spherical(gsl_vector*x){
/*	Test spherical funtion
	∫0π  dx/π ∫0π  dy/π ∫0π  dz/π [1-cos(x)cos(y)cos(z)]^-1 = Γ(1/4)4/(4π3) ≈ 
	1.3932039296856768591842462603255	*/
	double cosine = 1;
	for(int i=0; i< x->size; i++){
		cosine *= cos(gsl_vector_get(x,i)); /*	x²+y²+z²+...	*/
	}
/*	1/(1-cos) * 1/pi³	*/
	return pow(1-cosine,-1)*pow(M_PI,-3); 
}

double fun_cossine(double x, double y){
	return cos(x)*sin(y);
}