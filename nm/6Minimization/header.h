#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
/* Library from Linear Equations task. Including function definitions */
#include "../2LinearEquations/lineq.h"

/*	Functions	*/
double Rosenbrock(gsl_vector* p, gsl_vector* gradient);
double Himmelblau(gsl_vector* p, gsl_vector* gradient);

void Rosenbrock_hessian(gsl_vector * p, gsl_matrix * H);
void Himmelblau_hessian(gsl_vector * p, gsl_matrix * H);

double expo_decay(double t, gsl_vector *p); 
double expo_decay_min(gsl_vector *p, gsl_vector* gradient);

/*	Operations	*/
int newton(
double f(gsl_vector* p, gsl_vector* gradient),/* f: objective function, gradient: gradient, H: Hessian matrix H*/
void hessian(gsl_vector * p, gsl_matrix * H),
gsl_vector* x, /* starting point, becomes the latest approximation to the root on exit */
double accuracy);

int quasi_newton(
double f(gsl_vector* p, gsl_vector* gradient),/* f: objective function, gradient: gradient, H: Hessian matrix H*/
gsl_vector* x, /* starting point, becomes the latest approximation to the root on exit */
double accuracy);

/* 	Tools	*/

//void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
//void vector_print(const char* s,const gsl_vector* v);	// Prints vector
double vector_dot_prod(gsl_vector * x, gsl_vector * y);
void backtracking_linesearch(
	double f(gsl_vector* p, gsl_vector* gradient),
	gsl_vector * x, gsl_vector * Dx);
void backtracking_linesearch_qn(
	double f(gsl_vector* p, gsl_vector* gradient),
	gsl_matrix * H, gsl_vector * x, gsl_vector * Dx);
#define HAVE_HEADER_H
#endif