#include "stdio.h"
#include"gsl/gsl_matrix.h"

int print_half_00(gsl_matrix* m)
{
	double half = 0.5;
	int status = printf( "half m_{00} = %g\n", gsl_matrix_get(m,0,0)*half );
//	gsl_matrix_free(m);
	return status;
}

int main(void)
{
//printf("Hello, World!\n");
	
	gsl_matrix *m = gsl_matrix_alloc(2,2);
	gsl_matrix_set(m,0,0,66);
	printf("half m_{00} (should be 33):\n");

/* Upon successful return, these functions return the number of characters printed (excluding the
       null byte used to end output to strings).*/
	int status = print_half_00(m);
	printf("Status is the # of characters in the string.\n");
	if(status>0)
		printf("status=%i : everything went just fine (status>0)\n",status);
	else
		printf("status=%i : SOMETHING WENT TERRIBLY WRONG (status=0)\n",status);
	
	gsl_matrix_free(m);
	
return 0;
}