#include "header.h"

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n C. Implement a routine that integrates a function of two variables f(x,y)\n");
	fprintf(stderr, "_____________________________________________________________________________\n");

	double error;
	double eps = 1e-6, acc = 1e-6;
	double a = 0, b = M_PI;
	double c(double x){
		return x;
	}
	double d(double x){
		return pow(x,2);
	}

	double result = integrator_2D(fun_cossine,c,d,a,b,acc,eps,&error);
	fprintf(stderr,"Integral dx dy cos(x)*sin(y) from 0 to pi dx and x to x² dy \n");
	fprintf(stderr, " Calculated result %g\t Exact result 0.747023\n", result);

	return EXIT_SUCCESS;
}