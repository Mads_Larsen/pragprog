#include "header.h"

int main(){
	fprintf(stderr, "\n__________________________________________________\n");
	fprintf(stderr, " B. Quasi-Newton method with Broyden's update\n");
	fprintf(stderr, "__________________________________________________\n");

	int steps, n = 2;
	double f,accuracy = 1e-5;
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector * df= gsl_vector_alloc(n);

/*	Rosenbrock */
	gsl_vector_set_zero(x);
	fprintf(stderr, "\nRosenbrock minimum:\n");
	f = Rosenbrock(x,df);
	vector_print("Initial x", x);
	fprintf(stderr, "Rosenbrock(x) = %g\n",f);
	steps = quasi_newton(Rosenbrock, x, accuracy);
	f = Rosenbrock(x,df);
	vector_print("Rosenbrock minimum x", x);
	fprintf(stderr, "minimum Rosenbrock(x) = %g\n",f);
	fprintf(stderr, "steps = %i\n", steps );

/*	Himmelblau */
//	gsl_vector_set_zero(x); /* x = 0,0 finds maximum */
	gsl_vector_set(x,0,3);
	gsl_vector_set(x,1,1);
	fprintf(stderr, "\nHimmelblau minimum:\n");
	f = Himmelblau(x,df);
	vector_print("Initial x", x);
	fprintf(stderr, "Himmelblau(x) = %g\n",f);
	steps = quasi_newton(Himmelblau, x, accuracy);
	f = Himmelblau(x,df);
	vector_print("Himmelblau minimum x", x);
	fprintf(stderr, "minimum Himmelblau(x) = %g\n",f);
	fprintf(stderr, "steps = %i\n", steps );

/* 	Exponential Decay */
	fprintf(stderr, "\n Exponential decay parameter minimization\n");
	fprintf(stderr, "See fit in fit.svg\n");
	n = 3;
	gsl_vector * p = gsl_vector_alloc(n); /* Parameters */
	gsl_vector_set(p,0,3);
	gsl_vector_set(p,1,1);
	gsl_vector_set(p,2,3);

	vector_print("Exponential Decay initial parameters A,T,B", p);
	steps = quasi_newton(expo_decay_min, p, accuracy);
	vector_print("Exponential Decay minimum A,T,B", p);
	fprintf(stderr, "steps = %i\n", steps );

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	n = sizeof(t)/sizeof(t[0]);

//	fprintf(stdout,"time\tactivity\terror\n");
	for(int i=0; i<n; i++){
	fprintf(stdout,"%g\t%g\t%g\n",t[i],y[i],e[i]);
	}
	fprintf(stdout, "\n\n");

//	fprintf(stdout, "time\tactivity\n");
	for(int i = 0; i<100; i++){
		double t = 0.1*i;
		fprintf(stdout, "%g\t%g\n", t, expo_decay(t,p));
	}

	return EXIT_SUCCESS;
}	