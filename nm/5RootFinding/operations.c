#include "header.h"
void newton(void f(gsl_vector* x,gsl_vector* fx), 
			gsl_vector* x, double dx, double tolerance, gsl_vector* iter){

/*	Allocate Memory	*/
	int n=x->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);

	int funccall = 0, steps=0;

/*	Iterate untill satisfactory root x are found	*/
	do{
		funccall++;
		f(x,fx); /* calculates f(x) and puts it into vector fx */
		
	/*	Calculate Jacobian matrix using finite differences
		df_i / dx_k = (f(x+dx)-f(x)) / dx 	*/
		for (int i=0;i<n;i++){
			gsl_vector_set(x,i,
				gsl_vector_get(x,i)+dx); /* x[i] += dx */
			
			funccall++;
			f(x,df); /* df = f(x+dx) */
			gsl_vector_sub(df,fx); /* df=f(x+dx)-f(x) */
		
		/*	Generate Jacobian matrix */			
			for(int j=0;j<n;j++){
				gsl_matrix_set(J,j,i,
					gsl_vector_get(df,j)/dx);
			}
		/*	x <- x-dx	*/
			gsl_vector_set(x,i,
				gsl_vector_get(x,i)-dx);
		}
	/* 	Gram schmidt solve Newtons step,  Delta x
		J DeltaX = -fx	*/
		gs_decomp(J,Q,R);
		gs_solve(Q,R,fx,Dx);
		gsl_vector_scale(Dx,-1);
	
	/* 	backtracking line search where one ﬁrst attempts the full step,
		λ = 1, and then backtracks, λ ← λ/2, until the condition
		|| f(x + λ∆x)|| < (1− λ/2) ||f(x)||		*/
		double step_modulator=1, step_modulator_min = 0.02; /* Stepsize modulator, lambda */
		while(1){
			gsl_vector_memcpy(z,x); /* If step didn't work -> Go back to old x and try with new stepsize modulator */
			gsl_vector_add(z,Dx);
			funccall++;
			f(z,fz);
			/*	Stop loop if || f(x + λ∆x)|| < (1− λ/2) ||f(x)||	*/
			if( gsl_blas_dnrm2(fz)<(1-step_modulator/2)*gsl_blas_dnrm2(fx) || step_modulator < step_modulator_min ) break;
			step_modulator*=0.5; /* λ ← λ/2 */
			gsl_vector_scale(Dx,0.5);
			}
	/*	Copy x and fx values back 	*/
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
		steps++;
		} while(gsl_blas_dnrm2(Dx)>dx || gsl_blas_dnrm2(fx)>tolerance);
/*	Convergence criterion is ‖f(x)‖<ε,
	or if the step-size becomes smaller than the δx parameter.	*/

/*	Free Allocated Memory	*/
	gsl_matrix_free(J);		gsl_matrix_free(Q);	gsl_matrix_free(R);
	gsl_vector_free(fx);	gsl_vector_free(df);
	gsl_vector_free(fz);	gsl_vector_free(z);
	gsl_vector_free(Dx);

	gsl_vector_set(iter,0,steps);
	gsl_vector_set(iter,1,funccall);

}




void newton_jacobian(
	void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
	gsl_vector * x,	double tolerance, gsl_vector* iter
){

	/*	Allocate Memory	*/
	int n=x->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);

	int funccall = 0, steps=0;

/*	Iterate untill satisfactory root x are found	*/
	do{
		funccall++;
		f(x,fx, J); /* calculates f(x) and puts it into vector fx. Also inserts J */

/*	Notice the missing generation of Jacobian Matrix from before */

	/* 	Gram schmidt solve Newtons step,  Delta x
		J DeltaX = -fx	*/
		gs_decomp(J,Q,R);
		gs_solve(Q,R,fx,Dx);
		gsl_vector_scale(Dx,-1);
	
	/* 	backtracking line search where one ﬁrst attempts the full step,
		λ = 1, and then backtracks, λ ← λ/2, until the condition
		|| f(x + λ∆x)|| < (1− λ/2) ||f(x)||		*/
		double step_modulator=1, step_modulator_min = 0.02; /* Stepsize modulator, lambda */
		while(1){
			gsl_vector_memcpy(z,x); /* If step didn't work -> Go back to old x and try with new stepsize modulator */
			gsl_vector_add(z,Dx);
			funccall++;
			f(z,fz, J); /* The redefined J is not used... */
			/*	Stop loop if || f(x + λ∆x)|| < (1− λ/2) ||f(x)||	*/
			if( gsl_blas_dnrm2(fz)<(1-step_modulator/2)*gsl_blas_dnrm2(fx) || step_modulator < step_modulator_min ) break;
			step_modulator*=0.5; /* λ ← λ/2 */
			gsl_vector_scale(Dx,0.5);
			}
	/*	Copy x and fx values back 	*/
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
		steps++;
		} while(gsl_blas_dnrm2(fx)>tolerance);
/*	Convergence criterion is ‖f(x)‖<ε	*/

/*	Free Allocated Memory	*/
	gsl_matrix_free(J);		gsl_matrix_free(Q);	gsl_matrix_free(R);
	gsl_vector_free(fx);	gsl_vector_free(df);
	gsl_vector_free(fz);	gsl_vector_free(z);
	gsl_vector_free(Dx);

	gsl_vector_set(iter,0,steps);
	gsl_vector_set(iter,1,funccall);
}
