#include "exam.h"

void harm_osci(double x, gsl_vector * y, gsl_vector * dydx){
/*	Harmonic osc.. u''=-u -> y1=u, y2=u' -> y1'=y2, y2'=-y1	*/
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
}

void orbit(double x, gsl_vector * y, gsl_vector * dydx){
	double eps=0.01;

	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,
		1-gsl_vector_get(y,0)+eps*pow(gsl_vector_get(y,0),2));
}
