
#include "interpolation.h"


qspline * qspline_alloc(int n){ /* allocates and builds the quadratic spline */

	qspline* s = malloc(sizeof(qspline));//spline
	s->n = n;
	s->b = malloc((n-1)*sizeof(double));  // b_i
	s->c = malloc((n-1)*sizeof(double));  // c_i
	s->x = malloc(n*sizeof(double));      // x_i
	s->y = malloc(n*sizeof(double));      // y_i

	return s;
}

qspline * qspline_coeff(qspline *s, double *x, double *y){ /*	Creates b_i and c_i	*/
	
	for(int i=0;i<s->n;i++){
		s->x[i]=x[i];
		s->y[i]=y[i];
	}
	//s->x = x;
	//s->y = y;

	double dx[s->n];
	double dy[s->n];
/*	Set initial values	*/
	dy[0] = (s->y[1]-s->y[0]);
	dx[0] = (s->x[1]-s->x[0]);

	s->c[0] = 0;	/* Guess initial value */
	s->b[0] = dy[0]/dx[0];

/*	Recursive b_i and c_i	*/
	for (int i = 1 ; i < s->n-2 ; i++){

		dy[i] = ( s->y[i+1] - s->y[i] );
		dx[i] = ( s->x[i+1] - s->x[i] );

		s->b[i] = s->b[i-1] + 2* s->c[i-1] * dx[i-1];//(x[i]-x[i-1]);
		s->c[i] = dy[i]/(dx[i]*dx[i]) - s->b[i] / dx[i];

	}

/*	Do it from behind.
	To find proper limits	*/
	s->c[s->n-2] /= 2;
	s->b[s->n-2] = dy[s->n-2] / dx[s->n-2]
				-s->c[s->n-2] * dx[s->n-2];

	for (int i = s->n-3 ; i >= 0 ; i--){
		s->b[i] = 2*dy[i]/dx[i] - s->b[i+1];
		s->c[i] = dy[i]/(dx[i]*dx[i]) - s->b[i] / dx[i];
	}
	return s;
}

double qspline_evaluate(qspline *s, double z){       /* evaluates the prebuilt spline at point z */
	assert(z >= s->x[0] && z <= s->x[s->n-1]);
	
	int i = binary_search(s->n,s->x,z);
	double spline = s->y[i] + s->b[i] * (z - s->x[i])
							+ s->c[i] * ((z-s->x[i])*(z-s->x[i]));
	return spline;
}


double qspline_derivative(qspline *s, double z){ /* evaluates the derivative of the prebuilt spline at point z */
	assert(z >= s->x[0] && z <= s->x[s->n-1]);

	int i = binary_search(s->n,s->x,z);
	double spline_der = s->b[i]+ 2.0*s->c[i] * (z - s->x[i]);

	return spline_der;

	/* ???
	double spline_der = s->b[i]+ 2*s->c[i] * (z - s->x[i]); Doesnt work
	double spline_der = s->b[i]*1+ 2*s->c[i] * (z - s->x[i]); Works
	double spline_der = s->b[i]+ 2.0*s->c[i] * (z - s->x[i]); Works
	return = s->b[i]+ 2*s->c[i] * (z - s->x[i]); Works
	
	*/
}
double qspline_integral(qspline *s, double z){  /* evaluates the integral of the prebuilt spline from x[0] to z */
	assert(z >= s->x[0] && z <= s->x[s->n-1]);

	int i = 0;
	double spline_int = 0, dx;
	for(i = 0; s->x[i+1] < z; i++){
		dx = s->x[i+1] - s->x[i];

		spline_int += s->y[i]*dx + (1/2)*s->b[i]*(dx*dx) 
					+ (1/3)*s->c[i]*(dx*dx*dx);
	}
	double dxz = (z-s->x[i]);
	spline_int += s->y[i]*dxz + (1/2)*s->b[i]*(dxz*dxz) 
				+ (1/3)*s->c[i]*(dxz*dxz*dxz);
	return spline_int;
}
void qspline_free(qspline *s){ /* free memory allocated in qspline_alloc */
	free(s->b);
	free(s->c);
	free(s->x);
	free(s->y);
	free(s);
}