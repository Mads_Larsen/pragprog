#include "lineq.h"

void matrix_print(const char* s,const gsl_matrix* m){	// Prints matrix
	fprintf(stderr,"\n%s\n",s);
	for(int j=0;j<m->size1;j++){	// '->' dereferences m - size2 = column
		for(int i=0;i<m->size2;i++)	//	size1 = row
			fprintf(stderr,"%8.3f ",gsl_matrix_get(m,j,i));
		fprintf(stderr,"\n");
	}
}

void vector_print(const char* s,const gsl_vector* v){	// Prints vector
	fprintf(stderr,"\n%s\n",s);
	for(int i=0; i<v->size; i++){	// '->' dereferences m - size = row
			fprintf(stderr,"%8.3f ",gsl_vector_get(v,i));
		fprintf(stderr,"\n");
	}
}

void matrix_fill(gsl_matrix * A){ /*	Fill matrix 	*/
	for (int i = 0 ; i < A->size1 ; i++){
		for (int j = 0 ; j < A->size2 ; j++){
			gsl_matrix_set(A,i,j, rand() % 10+1); /* rnd # from 1-10 */
		}
	}
}

void vector_fill(gsl_vector * b){
	for (int i = 0 ; i < b->size ; i++){
		gsl_vector_set(b,i, rand() % 10+1);
	}
}
double matrix_col_inner_prod(gsl_matrix*A, int i, gsl_matrix*B, int j){
	assert(A->size1 == B->size1); /* Check they have the same size */
	double product = 0.0;
	int n = A->size1, l=0;

	for ( l = 0 ; l<n ; l++){
		product += gsl_matrix_get(A,l,i) * gsl_matrix_get(B,l,j);
	}
	return product;
}
