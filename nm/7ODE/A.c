#include "header.h"

void harm_osci(double x, gsl_vector * y, gsl_vector * dydx){
/*	Harmonic osc.. u''=-u -> y1=u, y2=u' -> y1'=y2, y2'=-y1	*/
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
}

int main(){
	fprintf(stderr, "\n____________________________________________________\n");
	fprintf(stderr, "\n A. Embedded Runge-Kutta ODE integrator\n");
	fprintf(stderr, "____________________________________________________\n");
	fprintf(stderr, " A.1 Implement an embedded Runge-Kutta stepper\n");
	fprintf(stderr, " A.2 Implement an adaptive-step-size driver routine\n");

	int n = 2;
	double a = 0, b = M_PI/2, h, abs = 1e-3, eps = 1e-3;
	gsl_vector*y = gsl_vector_alloc(n);

	gsl_vector_set(y,0,1);	/*	y(0)=1	*/
	gsl_vector_set(y,1,0);	/*	y'(0)=0	*/
	h = copysign(0.01,b-a);	/*	step direction dependent on sign	*/
    
	driver(&a,b,&h,y,abs,eps,&rkstep12,harm_osci);

	fprintf(stderr,"\nSolve y'' = -y for y(0) = 1 and y'(0)=0\n");
	fprintf(stderr,"Solved solution is\t y'(%g) = %g\ty(%g) = %g\n",
		a, gsl_vector_get(y,0), a, gsl_vector_get(y,1));
	fprintf(stderr,"Exact solution is\t cos(%g) = %g\t-sin(%g) = %g\n",
		a, cos(a), a, -sin(a));

	return EXIT_SUCCESS;
}