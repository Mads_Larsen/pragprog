#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
#include <time.h>

/*	Functions	*/
/* 	Tools	*/
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void matrix_fill_sym(gsl_matrix * A); /*	Fill matrix symetrically 	*/
void matrix_rebuild_sym(gsl_matrix * A);
void matrix_verify_sym(gsl_matrix * A); /*	Check if square and symmetric	*/

/*	Operations	*/
int jacobian_rotation(gsl_matrix*A, gsl_matrix*eigvec, gsl_vector*eigval, int p, int q, int sort);
int jacobian_sweep(gsl_matrix*A, gsl_matrix*eigval, gsl_vector*eigvec, int sort);
int jacobian_sweep_number(gsl_matrix*A, gsl_matrix*eigvec, gsl_vector*eigval, int number, int sort);

#define HAVE_HEADER_H
#endif