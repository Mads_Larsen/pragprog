#include "exam.h"

void matrix_print(const char* s,const gsl_matrix* m){	// Prints matrix
	fprintf(stderr,"\n%s\n",s);
	for(int j=0;j<m->size1;j++){	// '->' dereferences m - size2 = column
		for(int i=0;i<m->size2;i++)	//	size1 = row
			fprintf(stderr,"%8.3f ",gsl_matrix_get(m,j,i));
		fprintf(stderr,"\n");
	}
}

void vector_print(const char* s,const gsl_vector* v){	// Prints vector
	fprintf(stderr,"\n%s\n",s);
	for(int i=0; i<v->size; i++){	// '->' dereferences m - size = row
			fprintf(stderr,"%8.3f ",gsl_vector_get(v,i));
		fprintf(stderr,"\n");
	}
}
