#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
/* Library from Linear Equations task. Including function definitions */
#include "../2LinearEquations/lineq.h"

/*	Functions	*/
/* 	Tools	*/

/*	Operations	*/
void least_squares_fit(int m, double func(int i, double xi), 
	gsl_vector * x, gsl_vector * y, gsl_vector * dy,	/*	Data */
	gsl_vector * c, gsl_vector * c_error, gsl_matrix * covariance);

#define HAVE_HEADER_H
#endif