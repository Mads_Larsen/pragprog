#include <stdio.h>
#include <limits.h>
#include <float.h>

void sum(){

	// 2.
	printf("\n2.\n");
	int max = INT_MAX/2;
	int i;
	// 2.i
	float sum_up_float;
	float sum_down_float;
	i = 1;
	for(i; i<=max; i++){
		sum_up_float += 1.0f/i;
		sum_down_float += 1.0f / (max-i+1); // max -> 1
	}
	printf("2.i sum up and down using float\n");
	printf("sum_up_float =	%f\n", sum_up_float);
	printf("sum_down_float =	%f\n", sum_down_float);

	// 2.ii
	printf("\n2.ii Float precission is too low to hold exact sum value"
		", so there will be round off errors\n");

	// 2.iii
	printf("\n2.iii Double precision is higher than float."
		"Gives better results.\n");

	double sum_up_double;
	double sum_down_double;
	i = 1;
	for(i; i<=max; i++){
		sum_up_double += 1.0f/i;
		sum_down_double += 1.0f / (max-i+1); // max -> 1
	}
	printf("sum_up_double =	%f\n", sum_up_double);
	printf("sum_down_double =	%f\n", sum_down_double);	

	return 0;
}