#ifndef HAVE_LINEQ_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/* Functions	*/
/* Matrix operations */
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void matrix_fill(gsl_matrix * A); /* Fills matrix with rnd data */
double matrix_col_inner_prod(gsl_matrix*A, int i, gsl_matrix*B, int j);
void vector_print(const char* s,const gsl_vector* v);	// Prints vector

/* Vector operations */
void vector_fill(gsl_vector * b);
/* Assignments */
void gs_decomp(gsl_matrix * A, gsl_matrix * Q, gsl_matrix * R);
void gs_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x);
void gs_inverse(gsl_matrix *Q, gsl_matrix *R, gsl_matrix *B);

#define HAVE_LINEQ_H
#endif