#include "header.h"

//void rkstep12 (void f ( double t , double∗yt , double∗dydt) , 
//	double t , double∗ yt , double h , double∗ yth , double∗ dy){

void rkstep12(
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yt, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* yth,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
){

	int n = yt->size;
/*	Allocate memory	*/
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);
	
	gsl_vector_memcpy(yth,yt);

	f(t,yt,k0);
/*	daxpy: y = ax + y 	*/
	gsl_blas_daxpy(h/2, k0,  yt);	/* y_i+1 = y_i + k0_i * h/2 */
	f(t+h/2, yt, k12);
	gsl_blas_daxpy(h, k12, yth);	/* y_i+1 + h[i] = y_i + k12_i * h */

/*	err[i] = (k0[i] - k12[i]) * h/2;	*/
	gsl_vector_sub(k0,k12);
	gsl_vector_scale(k0,h/2);
	gsl_vector_memcpy(err,k0);

/*	Free Allocated Memory 	*/
	gsl_vector_free(k0); gsl_vector_free(k12);
}

/*
void driver(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double* step,gsl_vector* yx,double acc, double eps){
*/
void driver(
	double* t,                             /* the current value of the variable */
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	gsl_vector*yt,                         /* the current y(t) */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
){

	int iter = 0, n = yt->size;
	double tolerance;
	gsl_vector* yth = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);
	
	do{
		if (fabs(*t + *h) > fabs(b)) *h = b - *t;
		if(*t>=b) break;
		stepper(*t,*h,yt,f,yth,err);
	/*	tolerance = acc + eps * |yth|²	*/
		tolerance = (acc+eps*gsl_blas_dnrm2(yth));
		if(tolerance > gsl_blas_dnrm2(err)){
			gsl_vector_memcpy(yt,yth);
			iter++;
			*t += *h;
		}
	/*	h_i+1 = h_i × (τi /  ei)^Power × Safety	 */
		*h = *h*pow(tolerance/gsl_blas_dnrm2(err),0.25)*0.95;
	}while(iter < 1e3 );

	gsl_vector_free(yth); gsl_vector_free(err);
}

int driver_path(
	gsl_matrix* path,
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
){
/*	Define things	*/
	int n = path->size2-1, max_steps = path->size1, step = 0;
	double tolerance, t;
	gsl_vector* yth = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);
    gsl_vector* y = gsl_vector_alloc(n);

			
    /*	While t_i < b 	*/
	while(gsl_matrix_get(path,step,0) < b){
	/*	Set parameters t and y	*/
		t = gsl_matrix_get(path,step,0);
		for(int i=0; i<n; i++){
		gsl_vector_set(y,i,gsl_matrix_get(path,step,i+1));
		}
		
		if (fabs(t + *h) > fabs(b)) *h = b - t;
		if(t>=b) break;
		
		stepper(t,*h,y,f,yth,err);
		tolerance = (acc+eps*gsl_blas_dnrm2(yth));
		
		if(tolerance > gsl_blas_dnrm2(err)){
			step++;
			if(step+1 > max_steps){
				fprintf(stderr, "\n Max number of steps \n");
				break;
			}
		/*	Set t and y values in path matrix 	*/
			gsl_matrix_set(path, step, 0, t+ *h);
			for(int i=0; i<n; i++){
				gsl_matrix_set(path, step, i+1, gsl_vector_get(yth,i));
			}	
		}
		if( err > 0){
			*h = *h*pow(tolerance/gsl_blas_dnrm2(err),0.25)*0.95;
		}
		else *h=2;
	}

	gsl_vector_free(yth); gsl_vector_free(err);

	return step;
}