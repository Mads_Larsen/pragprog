#include "header.h"

void matrix_print(const char* s,const gsl_matrix* m){	// Prints matrix
	fprintf(stderr,"\n%s\n",s);
	for(int j=0;j<m->size1;j++){	// '->' dereferences m - size2 = column
		for(int i=0;i<m->size2;i++)	//	size1 = row
			fprintf(stderr,"%8.3f ",gsl_matrix_get(m,j,i));
		fprintf(stderr,"\n");
	}
}
void matrix_fill_sym(gsl_matrix * A){ /*	Fill matrix 	*/
	time_t t;
	srand((unsigned) time(&t));
	for (int i = 0; i < A->size1; i++){	/*	Row	*/
		for (int j = 0; j < A->size2; j++){	/*	Column */
			int rnd = rand() % 10+1; /* rnd # from 1-10 */
			gsl_matrix_set(A,i,j, rnd);
			gsl_matrix_set(A,j,i, rnd);
		}
	}
}

void matrix_rebuild_sym(gsl_matrix * A){
/*	Rebuild matrix upper triangular part from
			matrix lower triangular part.	*/
	for (int i = 0; i < A->size1; i++){	/*	Row	*/
		for (int j = 0; j < A->size2; j++){	/*	Column */
			gsl_matrix_set(A,i,j,
				gsl_matrix_get(A,j,i));
		}
	}
}

void matrix_verify_sym(gsl_matrix * A){
/*	Check if square and symmetric	*/
	assert(A->size1 == A->size2);
	for (int i = 0; i < A->size1; i++){	/*	Row	*/
		for (int j = 0; j < A->size2; j++){	/*	Column */
			if (gsl_matrix_get(A,i,j) != gsl_matrix_get(A,j,i)){
				fprintf(stderr, "\nMatrix not symmetric\n");
			}
		}
	}
}