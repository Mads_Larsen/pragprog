#include "header.h"

int jacobian_rotation(gsl_matrix*A, gsl_matrix*eigvec, gsl_vector*eigval, int p, int q, int sort){
	/* See if eigval_change=0 is needed */
	int eigval_change=0, dim = A->size1;

/*	Matrix elements	*/
	double aqq = gsl_vector_get(eigval, q);
	double app = gsl_vector_get(eigval, p);
	double apq = gsl_matrix_get(A, p, q);	

/*	If sort == 1 it sorts eigenvalues in descending order
	If sort != 1 it sorts eigenvalues in ascending order	*/
	double phi;
	if (sort == 1){
		phi = -0.5 * atan2(2 * apq, app - aqq);
	}else{
		phi = 0.5 * atan2(2 * apq, aqq - app);
	}

	double c = cos(phi);
	double s = sin(phi);
	double app1 = c * c * app - 2 * s * c * apq + s * s * aqq;
	double aqq1 = s * s * app + 2 * s * c * apq + c * c * aqq;

	if (app1 != app || aqq1 != aqq){
		eigval_change = 1;
		gsl_vector_set(eigval, p, app1);
		gsl_vector_set(eigval, q, aqq1);
		gsl_matrix_set(A, p, q, 0.0);

		for (int i = 0; i < p; i++){
			double aip = gsl_matrix_get(A, i, p);
			double aiq = gsl_matrix_get(A, i, q);
			double aip1 = c * aip - s * aiq;
			double aiq1 = c * aiq + s * aip;
			gsl_matrix_set(A, i, p, aip1);
			gsl_matrix_set(A, i, q, aiq1);
		}

		for (int i = p + 1; i < q; i++){
			double api = gsl_matrix_get(A, p, i);
			double aiq = gsl_matrix_get(A, i, q);
			double api1 = c * api - s * aiq;
			double aiq1 = c * aiq + s * api;
			gsl_matrix_set(A, p, i, api1);
			gsl_matrix_set(A, i, q, aiq1);
		}

		for (int i = q + 1; i < dim; i++){
			double api = gsl_matrix_get(A, p, i);
			double aqi = gsl_matrix_get(A, q, i);
			double api1 = c * api - s * aqi;
			double aqi1 = c * aqi + s * api;
			gsl_matrix_set(A, p, i, api1);
			gsl_matrix_set(A, q, i, aqi1);
		}
		for (int i = 0; i < dim; i++){
			double vip = gsl_matrix_get(eigvec, i, p);
			double viq = gsl_matrix_get(eigvec, i, q);
			gsl_matrix_set(eigvec, i, p, c * vip - s * viq);
			gsl_matrix_set(eigvec, i, q, c * viq + s * vip);
		}

	}

	return eigval_change;
}

int jacobian_sweep(gsl_matrix*A, gsl_matrix*eigvec, gsl_vector*eigval, int sort){
/*	Check if square and symmetric	*/
	matrix_verify_sym(A);
	gsl_matrix_set_identity(eigvec);
	int eigval_change, sweeps = 0, dim = A->size1;
	
	for (int k = 0; k < dim; k++){
		gsl_vector_set(eigval, k,
			gsl_matrix_get(A,k,k)
		);
	}

	do{
		sweeps++;
		eigval_change = 0;
		for (int p = 0; p < dim; p++){
			for (int q = p+1; q < dim; q++){
				eigval_change = jacobian_rotation(A,eigvec,eigval, p, q, sort);
			}
		}
	} while(eigval_change != 0 && sweeps < 1000);

	return sweeps;
}

int jacobian_sweep_number(gsl_matrix*A, gsl_matrix*eigvec, gsl_vector*eigval, int number, int sort){
/*	Check if square and symmetric	*/
	matrix_verify_sym(A);

	gsl_matrix_set_identity(eigvec);
	int eigval_change, sweeps = 0, dim = A->size1;
	
	for (int k = 0; k < dim; k++){
		gsl_vector_set(eigval, k,
			gsl_matrix_get(A,k,k)
		);
	}

	for (int p = 0; p <= number && p < dim; p++){
		do{
			sweeps++;
			eigval_change = 0;
			for (int q = p+1; q < dim; q++){
				eigval_change = jacobian_rotation(A,eigvec,eigval, p, q, sort);
			}
		} while(eigval_change != 0 && sweeps < 1000);
	} 

	return sweeps;
}