#include "exam.h"
/*	Two-Step Method	*/
void two_step(
	double tprev,
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yprev,									/* the previous point	*/
	gsl_vector* ynow, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* ynew,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
){
	int n = ynow->size;
	gsl_vector* yf = gsl_vector_alloc(n); 

	f(t,ynow,yf);	/* y'_i = f(x_i,y_i)	*/


/*	Find coefficient c 	*/
/*	c = (yi-1 - yi + y'(xi-xi-i))/(xi-xi-1)²	saved in yprev*/
	gsl_vector_sub(yprev,ynow);		/* yi-1 - yi 	-> yi-1	*/
	gsl_vector_scale(yf,(t-tprev));	/* y'*(xi-xi-1) -> y'	*/
	gsl_vector_add(yprev,yf);		/* yi-1 - yi + y'(xi-xi-i) -> yi-1	*/
	gsl_vector_scale(yprev,pow((t-tprev),-2));	/* yprev = c */

/*	Estimate error */
	gsl_vector_memcpy(err,yprev);	/*copy c (yprev) -> error	*/
	gsl_vector_scale(err,pow(h,2));

/*	Find second order function value	at t+h*/
	f(t,ynow,yf);						/* Reset y' = f(x_i,y_i)	*/
	gsl_vector_scale(yf,h);				/* y'*(t+h-t) = y'*h -> y'	*/
	gsl_vector_scale(yprev,pow(h,2));	/* c*(t+h-t)² -> c (yprev)	*/
	gsl_vector_add(yprev,yf);			/*y'*h + c*h² -> c (yprev)	*/
	gsl_vector_add(yprev,ynow);			/* yi + y'h + ch² -> c (yprev)	*/
	gsl_vector_memcpy(ynew,yprev);		/* copy yprev to ynew */

	gsl_vector_free(yf);
}

/*	Two-Step Method with Extra Evaluations	*/
void two_step_extra(
	double tprev,
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yprev,									/* the previous point	*/
	gsl_vector* ynow, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* ynew,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
){
	int n = ynow->size;
	gsl_vector* yf = gsl_vector_alloc(n); 
	gsl_vector* c  = gsl_vector_alloc(n);
	f(t,ynow,yf);	/* y'_i = f(x_i,y_i)	*/


/*	Find coefficient c 	*/
/*	c = (yi-1 - yi + y'(xi-xi-i))/(xi-xi-1)²	*/
	gsl_vector_sub(yprev,ynow);		/* yi-1 - yi 	-> yi-1	*/
	gsl_vector_scale(yf,(t-tprev));	/* y'*(xi-xi-1) -> y'	*/
	gsl_vector_add(yprev,yf);		/* yi-1 - yi + y'(xi-xi-i) -> yi-1	*/
	gsl_vector_scale(yprev,pow((t-tprev),-2));	/* yprev = c */
	gsl_vector_memcpy(yprev,c);


/*	Find second order function value	at t+h*/
	f(t,ynow,yf);						/* Reset y' = f(x_i,y_i)	*/
	gsl_vector_scale(yf,h);				/* y'*(t+h-t) = y'*h -> y'	*/
	gsl_vector_scale(yprev,pow(h,2));	/* c*(t+h-t)² -> c (yprev)	*/
	gsl_vector_add(yprev,yf);			/*y'*h + c*h² -> c (yprev)	*/
	gsl_vector_add(yprev,ynow);			/* yi + y'h + ch² = 2nd order -> yprev	*/
	/*	yprev is second order y value	*/

/*	Find coefficient d */
	double denominator = 2*(h)*(t+h-tprev)+pow(h,2);
	gsl_vector_scale(c,2*h);	/* 2c*(t+h-t)	*/
	f(t,yprev,ynew);			/* f(t,second order func) = y''=ft -> ynew	*/
	f(t,ynow,yf);				/* Reset y' = f(x_i,y_i)	*/
	gsl_vector_sub(yf,c);		/* y'-2ch -> y'	*/
	gsl_vector_sub(ynew,yf);	/* y'' - y' -> ynew	*/
	gsl_vector_scale(ynew,pow(denominator,-1));	/* numerator / denominator. d -> ynew */

/*	Find new third order functin value	*/
	gsl_vector_scale(ynew,pow(h,2)*(t+h-tprev));	/* d*h²*(ti+1 - ti-1)	*/
	gsl_vector_add(ynew,yprev);						/* 2nd order + ^ = 3rd order ynew	*/

/*	Estimate error */
	gsl_vector_memcpy(err,ynew);	/*copy ynew -> error	*/
	gsl_vector_sub(err,yprev);	/* ynew - yprev = error	*/

	gsl_vector_free(yf); gsl_vector_free(c);

}