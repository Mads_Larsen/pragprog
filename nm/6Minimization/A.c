#include "header.h"

int main(){
	fprintf(stderr, "\n____________________________________________________\n");
	fprintf(stderr, " A. Newton's minimization with gradiant and Hessian\n");
	fprintf(stderr, "____________________________________________________\n");


	int steps, n = 2;
	double f,accuracy = 1e-6;
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector * df= gsl_vector_alloc(n);
	gsl_vector_set_zero(x);

	fprintf(stderr, "\nRosenbrock minimum:\n");
	f = Rosenbrock(x,df);
	vector_print("Initial x", x);
	fprintf(stderr, "Rosenbrock(x) = %g\n",f);
	steps = newton(Rosenbrock, Rosenbrock_hessian, x, accuracy);
	f = Rosenbrock(x,df);
	vector_print("Rosenbrock minimum x", x);
	fprintf(stderr, "minimum Rosenbrock(x) = %g\n",f);
	fprintf(stderr, "steps = %i\n", steps );


//	gsl_vector_set_zero(x); /* x = 0,0 finds maximum */
	gsl_vector_set(x,0,0.-3);
	gsl_vector_set(x,1,0.-3);
	fprintf(stderr, "\nHimmelblau minimum:\n");
	f = Himmelblau(x,df);
	vector_print("Initial x", x);
	fprintf(stderr, "Himmelblau(x) = %g\n",f);
	steps = newton(Himmelblau, Himmelblau_hessian, x, accuracy);
	f = Himmelblau(x,df);
	vector_print("Himmelblau minimum x", x);
	fprintf(stderr, "minimum Himmelblau(x) = %g\n",f);
	fprintf(stderr, "steps = %i\n", steps );


	return EXIT_SUCCESS;
}	