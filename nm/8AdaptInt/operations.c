#include "header.h"

double integrator_recursion(
	double f (double) ,
	double a , double b , 
	double acc , double eps,
	double f2 , double f3 ,
	double *err, int recursions)
{
//fprintf(stderr, "2\n");
	assert(recursions < 1e6);
	double f1 = func(f,a + (b-a)/6, a, b);
	double f4 = func(f,a+5*(b-a)/6, a, b);
	double Q = (2*f1 + f2 + f3 + 2*f4)/6 * (b-a);
	double q = (  f1 + f2 + f3 +   f4)/4 * (b-a);
	double tolerance = acc+eps*fabs(Q);
	double error1, error2;
	*err = fabs(Q-q);

	if( *err < tolerance) return Q;
	else{
		double Q1 = integrator_recursion(
			f, a, (a+b)/2,
			acc/sqrt(2.0), eps,
			f1, f2,
			&error1, recursions+1);
		double Q2 = integrator_recursion(
			f, (a+b)/2, b,
			acc/sqrt(2.0), eps,
			f3, f4, 
			&error2, recursions+1);
		*err = sqrt(pow(error1,2) + pow(error2,2));
		Q = Q1+Q2;
		return Q;
	}
}

double integrator(
	double (*f)(double), 
	double a, double b,
	double acc, double eps, double *err)
{
	double f2 = func(f,a+2*(b-a)/6, a, b);
	double f3 = func(f,a+4*(b-a)/6, a, b);
	int recursions = 0;


	return integrator_recursion(	/* Q */
			f, a, b, acc, eps, f2, f3, err, recursions);

//	Q = my_estimate_of_the_integral;
//	*err = my_estimate_of_the_error; /* should be smaller than acc+|Q|*eps */
}

double integrator_limits(
	double f(double), 
	double a, double b,
	double acc, double eps, double *err)
{
/*	int _-inf ^inf 	*/
	if (isinf(a)==-1 && isinf(b)==1){
		double a_new = -1, b_new = 1;
		return integrator(f, a_new, b_new, acc, eps, err);
	}

/*	int _a ^inf 	*/
	else if(isinf(a)==0 && isinf(b)==1){
		double a_new = 0, b_new = 1;
		return integrator(f, a_new, b_new, acc, eps, err);
	}
/*	int _-inf ^ a 	*/
	else if(isinf(a)==-1 && isinf(b)==0){
		double a_new = -1, b_new = 0;
		return integrator(f, a_new, b_new, acc, eps, err);
	}
/*	If non-infinite	*/
	return integrator(f, a, b, acc, eps, err);
//	Q = my_estimate_of_the_integral;
//	*err = my_estimate_of_the_error; /* should be smaller than acc+|Q|*eps */
}


double integrator_Clenshaw_Curtis(
	double f(double x),
	double a, double b, 
	double acc, double eps, 
	double *err){

	a=0;
	b=M_PI;

	double f2 = func_Clenshaw_Curtis(f,a+2*(b-a)/6, a, b);
	double f3 = func_Clenshaw_Curtis(f,a+4*(b-a)/6, a, b);
	int recursions = 0;


	return integrator_recursion_Clenshaw_Curtis(	/* Q */
			f, a, b, acc, eps, f2, f3, err, recursions);
}


double integrator_recursion_Clenshaw_Curtis(
	double f (double) ,
	double a , double b , 
	double acc , double eps,
	double f2 , double f3 ,
	double *err, int recursions)
{
//fprintf(stderr, "2\n");
	assert(recursions < 1e6);
	double f1 = func_Clenshaw_Curtis(f,a + (b-a)/6, a, b);
	double f4 = func_Clenshaw_Curtis(f,a+5*(b-a)/6, a, b);
	double Q = (2*f1 + f2 + f3 + 2*f4)/6 * (b-a);
	double q = (  f1 + f2 + f3 +   f4)/4 * (b-a);
	double tolerance = acc+eps*fabs(Q);
	double error1, error2;
	*err = fabs(Q-q);

	if( *err < tolerance) return Q;
	else{
		double Q1 = integrator_recursion_Clenshaw_Curtis(
			f, a, (a+b)/2,
			acc/sqrt(2.0), eps,
			f1, f2,
			&error1, recursions+1);
		double Q2 = integrator_recursion_Clenshaw_Curtis(
			f, (a+b)/2, b,
			acc/sqrt(2.0), eps,
			f3, f4, 
			&error2, recursions+1);
		*err = sqrt(pow(error1,2) + pow(error2,2));
		Q = Q1+Q2;
		return Q;
	}
}

double func_Clenshaw_Curtis(double f(double), double theta, double a, double b){
	double x = 0.5*(a+b)+0.5*(a-b)*cos(theta);
	return f(x)*sin(theta)*(b-a)*0.5;
}

double func(double f(double), double x, double a, double b){
/*	int _-inf ^inf 	*/
	if (isinf(a)==-1 && isinf(b)==1){
		return f(x/(1-x*x))*(1+x*x)*pow(1-x*x,-2);
	}

/*	int _a ^inf 	*/
	else if(isinf(a)==0 && isinf(b)==1){
		return f( a + (x/(1-x)) * pow(1-x,-2) );
	}
/*	int _-inf ^ a 	*/
	else if(isinf(a)==-1 && isinf(b)==0){
		return f(x/(1+x*x))*(1+x*x)/(1+x*x);
	}
/*	If non-infinite	*/
	return f(x);
}