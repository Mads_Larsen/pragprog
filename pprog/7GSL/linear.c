#include <stdio.h>
//#include <stdlib.h>
// GSL libs
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_cblas.h>

void matrix_print(const char* s,const gsl_matrix* m){	// Prints matrix
	printf("\n%s\n",s);
	for(int j=0;j<m->size2;j++){	// '->' dereferences m - size2 = column
		for(int i=0;i<m->size1;i++)	//	size1 = row
			printf("%8.3g ",gsl_matrix_get(m,i,j));
		printf("\n");
	}
}
void vector_print(const char* s, const gsl_vector* v){
	printf("\n%s\n",s);
	gsl_vector_fprintf(stdout, v, "%g");

}
int main(){
	printf("\n2. SOLVING LINEAR SYSTEM\n");
	printf("Using Householder solver\n");

//	Writes data for matrix and vector in a readable way.
	double A_data[] = {	6.13,	-2.90,	5.86,
					  	8.98,	-6.31,	-3.89,
					  	-4.36,	1.00,	0.19,};

	double b_data[] = {	6.23,	5.37,	2.29};

	int n = 3;	// Dimension

//	Allocates memory for matrix A and vector b,x,y.
	gsl_matrix *A = gsl_matrix_calloc(n,n);
	gsl_matrix *A_copy = gsl_matrix_calloc(n,n);
	gsl_vector *b = gsl_vector_calloc(n);
	gsl_vector *x = gsl_vector_calloc(n);
	gsl_vector *y = gsl_vector_calloc(n);

	for(int i = 0; i < 3; i++){
		gsl_vector_set(b,i,b_data[i]);
		
		for(int j = 0; j<3; j++){
			gsl_matrix_set(A,i,j,A_data[3*i+j]);
		}
	}
//	Matrix A is destroyed in solve function. So a copy is made for comparison.
	gsl_matrix_memcpy(A_copy,A);

//	Prints matrixes to check them
	matrix_print("Matrix A", A);	// gsl_matrix_fprintf is ugly.
	vector_print("Vector b", b);



//	Linear
/*	Solves linear matrix system A*x = b. A_copy is destroyed,
	x is changed to the solution.*/
	gsl_linalg_HH_solve(A_copy, b, x);
	vector_print("solved vector x", x);

//	Multiply vectors A * x to check if they give b.
	gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0, y);
/*	d for double, ge for generel, mv for matrix-vector product
	the operation is y=aplha*A*b+beta*y, m matrix, b and y vectors.
	The CblasNoTrans means A isnt transponed. We don't want beta*y. 
	The 0 is the value of beta, and 1 is the value of alpha.*/

	vector_print("Multiplied vector A*x = y", y);

//	So this test says b and y aren't equal, but you can see that
//	they have the same values from the prints. Some odd gsl vector parameter?
	if(gsl_vector_equal(b,y)){
		printf("\ntest passed\n");
	}
	else{
		printf("\ntest failed\n");
	}
	printf("Functions are only equal to a certain precision\n");

//	Free dynamically allocated memory
	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(y);

	return 0;
}