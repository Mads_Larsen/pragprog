 #include "header.h"

void integrand(double x, gsl_vector * y, gsl_vector * dydx){
	gsl_vector_set(dydx,0, cos(x));	/* dydx = f(x)	*/
}

int main(){
	fprintf(stderr, "\n____________________________________________________\n");
	fprintf(stderr, "\n C. A definite integral as an ODE\n");
	fprintf(stderr, "____________________________________________________\n");

	int n = 1, max_steps = 1e3;
	double a = 0, b = M_PI/2, h, abs = 1e-3, eps = 1e-3;
	gsl_matrix* path = gsl_matrix_calloc(max_steps, n+1); /* [t y1...yn]	*/
	
	gsl_matrix_set(path,0,0, a);	/*	initial t 	*/
	gsl_matrix_set(path,0,1, 0);	/*	y(0)=1	*/
	h = copysign(0.01,b-a);	/*	step direction dependent on sign	*/
    
	int steps = driver_path(path,b,&h,abs,eps,&rkstep12,integrand);
	fprintf(stderr, "steps = %i\n", steps);
	fprintf(stderr, "solving y'(x)=f(x) for f(x) = cos(x)\n");
	fprintf(stderr , "Solved I = y(pi/2) = %g\n",
		gsl_matrix_get(path,steps,1));
	fprintf(stderr, "Exact  I = y(pi/2) = 1 \n");

	gsl_matrix_free(path);

	return EXIT_SUCCESS;
}