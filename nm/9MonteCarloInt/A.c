#include "header.h"

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n A. Plain Monte Carlo Integration\n");
	fprintf(stderr, "_____________________________________________________________________________\n");

	srand((unsigned) time(NULL));

	int dim = 3, N=1e6;
	gsl_vector *a = gsl_vector_calloc(dim);
	gsl_vector *b = gsl_vector_alloc(dim);
	double error; 
	double result;

/*	Test of x²+y²+z² from 0 to 1	*/
	gsl_vector_set_all(a,0);
	gsl_vector_set_all(b,1);

	MonteCarlo_plain(fun_square, a, b, N, dim,&result,&error); 
	fprintf(stderr,"\nIntegral over x²+y²+z² from x,y,z=0 to 1 with %i points\n", N);
	fprintf(stderr, " Calculated Result=%g\n Exact Result= %g\n Estimated Error= %g\n Actual Error= %g\n", 
		result, 1., error, fabs(result-1.));
	
/*	Test spherical funtion
	∫0π  dx/π ∫0π  dy/π ∫0π  dz/π [1-cos(x)cos(y)cos(z)]^-1 = Γ(1/4)4/(4π3) ≈ 
	1.3932039296856768591842462603255	*/

	N = 1e6; /* Starts getting pretty close at 1e8	*/
	gsl_vector_set_zero(a);
	gsl_vector_set_all(b,M_PI);
	double exact_result=pow(tgamma(1.0/4),4)/4.0/(M_PI*M_PI*M_PI);

	MonteCarlo_plain(fun_spherical, a, b, N, dim,&result,&error); 
	fprintf(stderr,"\nSpherical integral over ∫_0^π dx/π ∫_0^π dy/π ∫_0^π dz/π [1-cos(x)cos(y)cos(z)]^-1 with %i points:\n", N);
	fprintf(stderr, " Calculated Result=%g\n Exact Result= %.20lg\n Estimated Error= %g\n Actual Error= %g\n", 
		result, exact_result, error, fabs(result-exact_result));


	gsl_vector_free(a); gsl_vector_free(b);

	return EXIT_SUCCESS;
}