#include "header.h"

int main(){
	fprintf(stderr, "\n A. Ordinary least-squares fit by QR decomp\n");
/*	Data	*/	
	double x_data[]  = {0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9};
	double y_data[]  = {-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
	double dy_data[] = {1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};

/* 	We want to solve Rc = Q^T b, minimizing ||Rc-Q^Tb||^2
	Factorizing an n*m matrix A=QR	*/
	int m = 3;	/*	Number of functions	*/
	int n=sizeof(x_data)/sizeof(x_data[0]); /* array length. Sizeof returns byte size */

/*	Where A_ik = f_k(x_i) / dy_i and b_i = y_i / dy_i	*/
/*	Generate data vectors 	*/
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector * y = gsl_vector_alloc(n);
	gsl_vector * dy = gsl_vector_alloc(n);
	for (int i = 0; i<n; i++){
		gsl_vector_set(x,i,x_data[i]);
		gsl_vector_set(y,i,y_data[i]);
		gsl_vector_set(dy,i,dy_data[i]);
	}

/*	Define functions	*/
	double func(int i, double xi){
   		switch(i){
   			case 0:		return 	log(xi);	break;
   			case 1:		return 	1;		break;
   			case 2:		return 	xi;		break;
   			default:	return 	NAN;
   			}
		}
/*	We want to calculate the vector of the coefficients, c,
	where c is an m-component vector of unknown variables.
	Also find the covariance matrix	*/
	gsl_vector * c = gsl_vector_alloc(m);
	gsl_vector * c_error = gsl_vector_alloc(m);
	gsl_matrix * covariance = gsl_matrix_alloc(m,m);
	least_squares_fit(	m, func, 
							x, y, dy,	/*	Data */
							c, c_error, covariance);
	fprintf(stderr, "Coefficients c\n");
	gsl_vector_fprintf(stderr,c,"%g");

/*	B Uncertainties of the fitting coefficients */
	fprintf(stderr, "\n B. Uncertainties of the fitting coefficients\n");

/*	c error from covariance matrix 	*/

	matrix_print("Covariance matrix", covariance);
	vector_print("c error", c_error);
	fprintf(stderr, "\nSee the fit to data in fit.svg\n");

/*	Find the error of the c values	*/
//	gsl_vector * c_error = gsl_vector_alloc(m);
//	fprintf(stdout, "x\ty\tdy\n");
	for (int i = 0; i<n; i++){
		fprintf(stdout, "%8.5g\t%8.5g\t%8.5g\n",
			x_data[i], y_data[i], dy_data[i] );
	} fprintf(stdout, "\n\n");
	fprintf(stdout, "x\tfit\tfit_plus\tfit_minus\n");
	for (int i = 0; i<n; i++){
		double fit=0;
		double F_error = 0;
		for (int j = 0; j<m; j++){
			fit += gsl_vector_get(c,j) * func(j, x_data[i]);
			F_error += pow(gsl_vector_get(c_error,j)*func(j,x_data[i]),2);
		}
		F_error = sqrt(F_error);
		double fit_plus = fit + F_error;
		double fit_minus = fit- F_error;

		fprintf(stdout, "%8.5g%8.5g %8.5g %8.5g\n",
			x_data[i],fit,fit_plus,fit_minus );

	}

//	fprintf(stderr, "\nError of c values\n");
//	gsl_vector_fprintf(stderr,c_error,"%g");


	return EXIT_SUCCESS;
}