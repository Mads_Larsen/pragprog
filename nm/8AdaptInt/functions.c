#include "header.h"												/* int_0^1 fx */

//double func_sqrt 	(double x){evals++; return sqrt(x);}					/* = 2/3	*/
//double func_invsqrt	(double x){evals++; return pow(sqrt(x),-1);}			/* = 2		*/
//double func_logsqrt	(double x){evals++; return log(x)*pow(sqrt(x),-1);}	/* =-4		*/
//double func_pi		(double x){evals++; return 4*sqrt(1-pow(1-x,2));}	/* = pi 	*/

/* Infinite limits functions	*/
double func(double f(double), double x, double a, double b){
/*	int _-inf ^inf 	*/
	if (isinf(a)==-1 && isinf(b)==1){
		return f(x/(1-x*x))*(1+x*x)*pow(1-x*x,-2);
	}

/*	int _a ^inf 	*/
	else if(isinf(a)==0 && isinf(b)==1){
		return f( a + (x/(1-x)) * pow(1-x,-2) );
	}
/*	int _-inf ^ a 	*/
	else if(isinf(a)==-1 && isinf(b)==0){
		return f(x/(1+x*x))*(1+x*x)/(1+x*x);
	}
/*	If non-infinite	*/
	return f(x);
}