#include "montecarlo.h"

void randomx(int dim, gsl_vector*a, gsl_vector*b, gsl_vector*x, gsl_rng *RNG){
	for(int i=0; i<dim; i++){
//		x[i] = a[i] + RND*(b[i]-a[i]);
		gsl_vector_set(x,i,
			gsl_vector_get(a,i) +
			gsl_rng_uniform(RNG)*( gsl_vector_get(b,i) - gsl_vector_get(a,i) ) );
	/* gsl_RNG_uniform returns random number [0,1]	*/
	}
}

void MonteCarlo_plain(
	double f(gsl_vector*x), 
	gsl_vector*a, gsl_vector*b, 
	int N, int dim,
	double*result, double*error){

	double volume = 1;
	for(int i=0; i<dim; i++){
//		volume*=b[i]-a[i];
		volume *= gsl_vector_get(b,i)-gsl_vector_get(a,i);
	}

	double sum11=0, sum12=0, sum21=0, sum22=0 ,fx;
	gsl_vector *x = gsl_vector_alloc(dim);

/*	GSL Random number generator	*/
	gsl_rng * RNG1 = gsl_rng_alloc(gsl_rng_ranlux389);
	gsl_rng * RNG2 = gsl_rng_alloc(gsl_rng_ranlux389);
/*	Choose different seeds	*/
	gsl_rng_set(RNG1,1);
	gsl_rng_set(RNG2,2);
	#pragma omp parallel sections
	{
	//for(int i=0; i<threads; i++)
		#pragma omp section
		{
			for (int i=0; i<N/2; i++){
				randomx(dim,a,b,x, RNG1);
				fx = f(x);
				sum11+=fx;
				sum12+=pow(fx,2);
			}
		}
		#pragma omp section
		{
			for (int i=N/2; i<N; i++){
				randomx(dim,a,b,x, RNG2);
				fx = f(x);
				sum21+=fx;
				sum22+=pow(fx,2);
			}
		}
	}
	double average = (sum11+sum21)/N;
	double variance = (sum12+sum22)/N - pow(average,2);

	*result = average*volume;
	*error = sqrt(variance/N)*volume;

	gsl_vector_free(x);
	gsl_rng_free(RNG1); gsl_rng_free(RNG2);

}