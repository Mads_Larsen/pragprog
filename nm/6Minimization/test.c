double expoDecay(gsl_vector *p, gsl_vector* gradient) {
	double A = gsl_vector_get(p,0);
	double T = gsl_vector_get(p,1);
	double B = gsl_vector_get(p,2);

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int n = sizeof(t)/sizeof(t[0]);


	double result =0, grad1 = 0, grad2 = 0, grad3 = 0, temp;
	for( int i = 0; i < n; i++) {
		temp = 2*(A*exp(-t[i]/T)+B-y[i])/pow(e[i],2);
		grad1 += exp(-t[i]/T)*temp;
		grad2 += A*t[i]/pow(T,2)*exp(-t[i]/T)*temp;
		grad3 += temp;
	/*	The (master) function to minimize is F(A,T,B)=∑i(f(ti)-yi)²/σi² */
		master_func += pow( (A*exp(-t[i]/T)+B-y[i])/e[i] , 2 );
	}
	gsl_vector_set(gradient,0,grad1);
	gsl_vector_set(gradient,1,grad2);
	gsl_vector_set(gradient,2,grad3);

	return master_func;
}
