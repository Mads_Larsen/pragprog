#include "header.h"
#include <gsl/gsl_multiroots.h>



/*Find the minimum of the function,

by finding the root of it's gradient (that is, 
the point where the gradient is zero). */

int root_gsl (int func(const gsl_vector* x, void * params,
 gsl_vector* fx, gsl_matrix * J),
	gsl_vector * x){

	int dim = 2;

	/*	Defines function */
	gsl_multiroot_function_fdf f;
	f.fdf = func;
	f.n = dim;
	f.params = NULL;
/*	The solver s is initialized to use this function, 
	with the gsl_multiroot_fsolver_hybrids dnewton, for comparison*/
	const gsl_multiroot_fdfsolver_type *T = gsl_multiroot_fdfsolver_newton;
	gsl_multiroot_fdfsolver *s = gsl_multiroot_fdfsolver_alloc (T, dim);

/* 	Set, or reset, an existing solver s 
	to use the function f or function and derivative fdf*/
	gsl_multiroot_fdfsolver_set (s, &f, x);

	int status, iter = 0;
	double acc = 1e-7;
	do{
	  iter++;
	  status = gsl_multiroot_fdfsolver_iterate (s);

	  if (status)   /* check if solver is stuck */
	    break;
	  status = gsl_multiroot_test_residual (s->f, acc);
	}
	while (status == GSL_CONTINUE && iter < 1000);
//	fprintf (stderr,"status = %s\n", gsl_strerror (status));

	gsl_vector_memcpy(x,s->x);

/*	Free allocated memory */
	gsl_multiroot_fdfsolver_free (s);
	return iter;
}