#include "header.h"

int main(){
	fprintf(stderr, "\n B. Jacobi diagonalization eigenvalue-by-eigenvalue\n");

/*	Square symmetric matrix */
	int n = 5;	/* Dimension */
	gsl_matrix * A = gsl_matrix_alloc(n,n);
	gsl_matrix * eigvec = gsl_matrix_alloc(n,n);
	gsl_vector * eigval = gsl_vector_alloc(n);
	gsl_matrix * eigval_list = gsl_matrix_alloc(n,n);
	gsl_vector * sweeps = gsl_vector_alloc(n);

/*	Fill A matrix and copy it */
	matrix_fill_sym(A);

/*	Calculate down to the i'th eigenvalue	*/
	for(int i = 0; i <n; i++){
//		sweeps[i] += jacobian_sweep_number(A, eigvec, eigval,3);
		gsl_vector_set(sweeps, i, 
			jacobian_sweep_number(A, eigvec, eigval,i, 0)
		);
		gsl_matrix_set_col(eigval_list, i, eigval);
		matrix_rebuild_sym(A);
	}
/*	1. */
	fprintf(stderr, "\n 1. Calculate the given number of lowest eigenvalues\n");
	fprintf(stderr, "\nEigenvalues asked to evaluate\n\t1\t2\t3\t4\t5\n");
	fprintf(stderr, "sweeps\n\t%g\t%g\t%g\t%g\t%g\n",
		gsl_vector_get(sweeps,0),
		gsl_vector_get(sweeps,1),
		gsl_vector_get(sweeps,2),
		gsl_vector_get(sweeps,3),
		gsl_vector_get(sweeps,4));
	matrix_print("Eigenvalues", eigval_list);
	fprintf(stderr, "\nNotice how it calculates the last 2 eigenvalues in the 2nd last evaluation. \n");

/*	2. 	change the formulas for the rotation angle to obtain the largest 
		eigenvalues instead of the lowest.	*/ 
	fprintf(stderr, "\n 2. Change rotation angle to sort descending rather than ascending\n");
	fprintf(stderr, "\nCalculate reverse rotation matrix\n");
	jacobian_sweep(A,eigvec, eigval, 1);
	fprintf(stderr,"Descending eigenvalues\n");
	gsl_vector_fprintf(stderr, eigval, "%g");

/*	Free allocated memory	*/
	gsl_matrix_free(A);	
	gsl_matrix_free(eigvec); 
	gsl_vector_free(eigval); gsl_matrix_free(eigval_list);

/*	3. Compare sweeps	*/
	fprintf(stderr, "\n 3. Compare # of sweeps for cyclic and lowest eigenvalue\n");

/*	Square symmetric matrix, eigenvector and Eigenvalues 	*/
	int m = 100;	/* Dimension	*/
	A = gsl_matrix_alloc(m,m);
	eigvec = gsl_matrix_alloc(m,m);
	eigval = gsl_vector_alloc(m);
	matrix_fill_sym(A);

/*	Diagonalize using cyclic and lowest eigenvalue. compare sweeps */
	fprintf(stderr, "\nMatrix size %i x %i\n",m,m);
	int i=0;
	do{
		if(i==0){
			fprintf(stderr, "\nAscending\n");
		}else{
			fprintf(stderr, "\nDescending\n");
		}

		int sweeps_cyclic = jacobian_sweep(A,eigvec,eigval, i);
		matrix_rebuild_sym(A);
		int sweeps_lowest = jacobian_sweep_number(A,eigvec, eigval,0,i);
		matrix_rebuild_sym(A);
		int sweeps_full = jacobian_sweep_number(A,eigvec, eigval,m,i);
		matrix_rebuild_sym(A);
		fprintf(stderr,"Cyclic sweeps = %i\tlowest sweeps = %i\t Fully one by one sweeps = %i\n",
		sweeps_cyclic, sweeps_lowest, sweeps_full);
		i++;
	}while(i <= 1);
	return EXIT_SUCCESS;
}