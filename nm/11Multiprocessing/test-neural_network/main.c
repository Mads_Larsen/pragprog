#include "neural.h"

double activation_function(double x){return x*exp(-x*x);}
double function_to_fit(double x, double y){
	return exp(-x*x - y*y);
}

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n A. Multi-threading version of 2D Artificial Neural Network using openMP\n");
	fprintf(stderr, "_____________________________________________________________________________\n");


/*	Training values 	*/
	double x_min = -2, x_max = 2,	y_min = -2, y_max = 2;
	int training_points=20;
	gsl_matrix* params_train=gsl_matrix_alloc(pow(training_points,2),2);
	gsl_vector* f_train=gsl_vector_alloc(pow(training_points,2));

	double dx = fabs(x_max-x_min)/(training_points-1);
	double dy = fabs(y_max-y_min)/(training_points-1);

/*	Set training values	*/
	for(int i=0; i<training_points; i++){
		double xval = x_min + i*dx;
		for(int j=0; j<training_points; j++){
			int index = i*training_points+j;
			double yval = y_min + j*dy;
			double fxy = function_to_fit(xval,yval);

			gsl_matrix_set(params_train, index, 0, xval);
			gsl_matrix_set(params_train, index, 1, yval);
			gsl_vector_set(f_train, index, fxy);

		}
	}

/*	Neural Network	*/
	int number_neurons=2*training_points;
	neuron* network=neuron_alloc_2D(number_neurons,&activation_function);

/*	Set initial guess for parameters	a&b for x, x&d for y*/
	for(int i=0; i<network->n; i++){
		double normalize = i/(network->n-1.0);
		gsl_vector_set(network->data,5*i+0, x_min+(x_max-x_min)*normalize); /*a*/
		gsl_vector_set(network->data,5*i+1, 1);	/*b*/
		gsl_vector_set(network->data,5*i+2, y_min+(y_max-y_min)*normalize);	/*c*/
		gsl_vector_set(network->data,5*i+3, 1);	/*d*/
		gsl_vector_set(network->data,5*i+4, 1);	/*weight*/
	}

/*	Find paramseters minimizing the deviation, using training data */
	neuron_train_2D(network,params_train,f_train);

/*	Print training values	*/
	for(int i=0; i<params_train->size1; i++){
		double x=gsl_matrix_get(params_train,i,0);
		double y=gsl_matrix_get(params_train,i,1);
		double f=gsl_vector_get(f_train,i);
		fprintf(stdout,"%g %g %g\n",x,y,f);
	}
	fprintf(stdout,"\n\n");

/*	Generate output values using trained parameters, and print them	*/
	double step=1.0/5.0	;
	for(double x=x_min; x<=x_max; x+=step){
		for(double y = y_min; y<y_max; y+=step){ 
			double output=neuron_feed_forward_2D(network,x,y);
			fprintf(stdout,"%g %g %g\n",
				x, y,output);
		}
	}
	fprintf(stdout, "\n\n");

/*	Print to log	*/
	fprintf(stderr, "\n");
	fprintf(stderr, "Number of training points: %i\n", training_points );
	fprintf(stderr, "Number of Neurons: %i\n", number_neurons );
	fprintf(stderr, "Fitting function exp(-x²-y²)\n");
	fprintf(stderr, "See trained fit in Fig. interpolation_2D.svg\n");

	neuron_free(network);


	return EXIT_SUCCESS;
}