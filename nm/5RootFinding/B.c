#include "header.h"

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n B. Newton's method with analytic Jacobian\n");
	fprintf(stderr, "_____________________________________________________________________________\n");


	int n = 2;
	double  tolerance = 1e-3;
	gsl_vector * iter = gsl_vector_alloc(n); /* steps & func calls */
	gsl_vector * fx= gsl_vector_alloc(n);
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,2);
/*	Can't be solved for initial x=[0,0] as it'll divide by zero?	*/

	fprintf(stderr,"\nExtremum of the System of Equations:\n");

	System_of_Equations(x,fx); /* There's no need to replace this with _Jacobian */
	vector_print("initial vector x: ",x);
	vector_print("System of Equations(x): ",fx);
	newton_jacobian(System_of_Equations_Jacobian,x,tolerance, iter);
	vector_print("solution x: ",x);
	System_of_Equations(x,fx);
	vector_print("System_of_Equations(x) solved: ",fx);
	fprintf(stderr, "Steps = %g, function calls = %g\n",
		gsl_vector_get(iter,0), gsl_vector_get(iter,1) );


	fprintf(stderr,"\nExtremum of the Rosenbrock's function:\n");
	gsl_vector_set_zero(x);
	Rosenbrock(x,fx);	
	vector_print("initial vector x: ",x);
	vector_print("Rosenbrock(x): ",fx);
	newton_jacobian(Rosenbrock_Jacobian,x,tolerance, iter);
	vector_print("solution x: ",x);
	Rosenbrock(x,fx);
	vector_print("Rosenbrock(x) solved: ",fx);	
	fprintf(stderr, "Steps = %g, function calls = %g\n",
		gsl_vector_get(iter,0), gsl_vector_get(iter,1) );

	fprintf(stderr,"\nExtremum of the Himmelblau's function:\n");
	gsl_vector_set_zero(x);
	Himmelblau(x,fx);	
	vector_print("initial vector x: ",x);
	vector_print("Himmelblau(x): ",fx);
	newton_jacobian(Himmelblau_Jacobian,x,tolerance, iter);
	vector_print("solution x: ",x);
	Himmelblau(x,fx);
	vector_print("Himmelblau(x) solved: ",fx);	
	fprintf(stderr, "Steps = %g, function calls = %g\n",
		gsl_vector_get(iter,0), gsl_vector_get(iter,1) );

	fprintf(stderr, "\nNotice how the method with predefined analytical Jacobian matrices requires fewer steps in general\n");

	fprintf(stderr, "\n Comparison with GSL root finding, using Newtonian method and analytical Jacobian Matrix\n");

int iter1;

	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,2);
	iter1 = root_gsl(System_of_Equations_Jacobian_gsl,x);
	vector_print("solution x: ",x);
	System_of_Equations(x,fx);
	vector_print("System_of_Equations(x) solved: ",fx);	
	fprintf(stderr, "Steps = %i\n",
		iter1);

	gsl_vector_set_zero(x);
	iter1 = root_gsl(Rosenbrock_Jacobian_gsl,x);
	vector_print("Rosenbrock solution x: ",x);
	Rosenbrock(x,fx);
	vector_print("Rosenbrock(x) solved: ",fx);	
	fprintf(stderr, "Steps = %i\n",
		iter1);

	gsl_vector_set_zero(x);
	iter1 = root_gsl(Himmelblau_Jacobian_gsl,x);
	vector_print("Himmelblau solution x: ",x);
	Himmelblau(x,fx);
	vector_print("Himmelblau(x) solved: ",fx);	
	fprintf(stderr, "Steps = %i\n",
		iter1);
	fprintf(stderr, "\nIt can be seen, that GSL root solving is faster\n");

	return 	EXIT_SUCCESS;
}