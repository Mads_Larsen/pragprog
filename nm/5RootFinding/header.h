#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
/* Library from Linear Equations task. Including function definitions */
#include "../2LinearEquations/lineq.h"

/*	Functions	*/
void System_of_Equations(gsl_vector* p, gsl_vector* fx);
void Rosenbrock(gsl_vector* p, gsl_vector* fx);
void Himmelblau(gsl_vector* p, gsl_vector* fx);

void System_of_Equations_Jacobian(gsl_vector* p, gsl_vector* fx, gsl_matrix* J);
void Rosenbrock_Jacobian(gsl_vector* p, gsl_vector* fx, gsl_matrix* J);
void Himmelblau_Jacobian(gsl_vector* p, gsl_vector* fx, gsl_matrix* J);

int System_of_Equations_Jacobian_gsl(const gsl_vector* p, void *params, gsl_vector* fx, gsl_matrix* J);
int Rosenbrock_Jacobian_gsl(const gsl_vector* p, void *params, gsl_vector* fx, gsl_matrix* J);
int Himmelblau_Jacobian_gsl(const gsl_vector* p, void *params, gsl_vector* fx, gsl_matrix* J);

/* 	Tools	*/

/*	Operations	*/
void newton(
	void f(gsl_vector* x, gsl_vector* fx),
	gsl_vector * x,
	double dx, double tolerance, gsl_vector* iter
);

void newton_jacobian(
	void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J),
	gsl_vector * x,	double tolerance, gsl_vector* iter
);

int root_gsl (int func(const gsl_vector* x, void * params, gsl_vector* fx,gsl_matrix* J),
	gsl_vector * x);

#define HAVE_HEADER_H
#endif