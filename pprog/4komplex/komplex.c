#include <stdio.h>
#include "komplex.h"
#include <tgmath.h>

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_mul (komplex a, komplex b){
	komplex result_mul = {a.re*b.re - a.im*b.im, a.im*b.re + a.re*b.im};
	return result_mul;
}


/* ... */
int double_equal (double a, double b, double TAU, double EPS){
	if (fabs(a-b)< TAU || fabs(a-b)<EPS* (fabs(a) + fabs(b)))
		return 1;
	else
		return 0;
	}

void komplex_equal(komplex a, komplex b, double TAU, double EPS){
		/* code */
	if (double_equal(a.re, b.re, TAU, EPS) && double_equal(a.im, b.im, TAU, EPS))
		printf("test passed :) \n");
	else
		printf("test failed: debug me, please... \n");

/* Changed from int to void to save space in main.c - Should use int
if i want to use the true/false information in the code.*/
}