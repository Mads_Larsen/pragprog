#ifndef HAVE_EXAM_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/*	Functions	*/
void harm_osci(double x, gsl_vector * y, gsl_vector * dydx);
void orbit(double x, gsl_vector * y, gsl_vector * dydx);

/*	Step Operations	*/
void two_step(
	double tprev,										/* the previous step */
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yprev,									/* the previous point	*/
	gsl_vector* ynow, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* ynew,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
);

void two_step_extra(
	double tprev,										/* the previous step */
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yprev,									/* the previous point	*/
	gsl_vector* ynow, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* ynew,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
);

void rkstep12(
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* ynow, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* ynew,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
);

/* Drivers	*/
int adaptive_driver(
	double* t,                             /* the current value of the variable */
	double b,                              /* the end-point of the integration */
	double* h,                             /* the initial step-size */
	gsl_vector*ynow,                         /* the current y(t) */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double tprev, double t, double h,
		gsl_vector* yprev, gsl_vector*ynow,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*ynew, gsl_vector*err
		),
	void f(double t,gsl_vector*y,gsl_vector*dydt), /* right-hand-side */
	gsl_vector* err
);

int adaptive_driver_path(
	gsl_matrix* path,
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double tprev, double t, double h, 
		gsl_vector*yprev, gsl_vector*ynow,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*ynew, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
);

/* 	Tools	*/
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void vector_print(const char* s,const gsl_vector* v);	// Prints vector

#define HAVE_EXAM_H
#endif