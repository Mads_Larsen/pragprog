#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h> // For GSL_SUCCESS 
#include <gsl/gsl_odeiv2.h>
#define steptype gsl_odeiv2_step_rkf45

/* This function should store the vector elements f_i(x,y,params) in the array f,
 for arguments (x, y) and parameters params.
	y'(x) = y(x)*(1-y(x))
 */
int func( double x, const double y[], double dydx[], void * params){
	(void)(x);	// Avoid unused parameter warning
	dydx[0] = y[1];
	dydx[0] = y[0] * (1-y[0]);

	return GSL_SUCCESS;
}

double ODEfunc(double x){

//	printf("Hello, world!\n");
	gsl_odeiv2_system sys;
	sys.function = func;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = NULL;

/*	Pointer to newly allocated driver object.
	Driver automatically allocates & initializes the evolve, control & stepper
	objexts for the ODE system 'sys'*/

	const double hstart = copysign(0.1, x);
	const double accuracy = 1e-6;
	const double eps = 0.0;

	gsl_odeiv2_driver * driver = gsl_odeiv2_driver_alloc_y_new (
							&sys, steptype, hstart, accuracy, eps);

	double xi = 0.0; 		// Initial x
	double y[2] = {0.5,0}; 	// Initial y condition
	gsl_odeiv2_driver_apply (driver, &xi, x, y);

	gsl_odeiv2_driver_free (driver);	// Free allocated driver

	return y[0];	// Function ODEfunc returns the new y value
}

int main(){
	printf("x\ty\tlogistic \n");
	for(double x = 0;	x <= 3+0.1; x+=0.1){

		double logistic = 1/(1+exp(-x));

		printf("%g\t%g\t%g\n", x, ODEfunc(x), logistic);
	}
}