#include <stdio.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>

int
main (void)
{

  printf("\n3. EIGENVALUES & EIGENVECTORS OF 4TH ORDER HILBERT MATRIX\n");

  int n=4;
// Initialize random 4th order matrix.  
  gsl_matrix* M=gsl_matrix_calloc(n,n);
  for(int i=0;i<M->size1;i++){
    for(int j=0;j<M->size2;j++){
      gsl_matrix_set(M,i,j,1/(i+j+1));
    }
  }


  gsl_vector *eval = gsl_vector_alloc (n);
  gsl_matrix *evec = gsl_matrix_alloc (n, n);

  gsl_eigen_symmv_workspace * w =
    gsl_eigen_symmv_alloc (n);

  gsl_eigen_symmv (M, eval, evec, w);

  gsl_eigen_symmv_free (w);

gsl_eigen_symmv_sort (eval, evec,
                        GSL_EIGEN_SORT_ABS_ASC);
  {
    printf("Eigenvalue \t eigenvector\n");
    for (int i = 0; i < n; i++)
      {
      double eval_i
        = gsl_vector_get (eval, i);
      gsl_vector_view evec_i
        = gsl_matrix_column (evec, i);



      printf ("eigenvalue = %g\n", eval_i);
      printf ("eigenvector = \n");
      gsl_vector_fprintf (stdout, &evec_i.vector, "%g");
      }
  }

  gsl_matrix_free(M);
  gsl_vector_free (eval);
  gsl_matrix_free (evec);

  return 0;
}