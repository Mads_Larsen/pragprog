#include "interpolation.h"

/* Allocates memory for cubic spline 	*/
cubic_spline *cubic_spline_alloc(int n){
	
	cubic_spline *s = (cubic_spline*) malloc(sizeof(cubic_spline));
	
	s->n = n;
	
	s->x = (double*) malloc( n    * sizeof(double));
	s->y = (double*) malloc( n 	  *	sizeof(double));

	s->b = (double*) malloc( n    * sizeof(double));
	s->c = (double*) malloc((n-1) * sizeof(double));
	s->d = (double*) malloc((n-1) * sizeof(double));

	return s;
}

/* Generate coefficients of the cubic spline 	*/
cubic_spline * cubic_spline_coeff(cubic_spline *s, double *x, double *y){
	int i=0;

/*	define x and y	*/
	for (i = 0 ; i < (s->n) ; i++){
		s->x[i] = x[i];
		s->y[i] = y[i];
	}

/*	VLA	*/
	double dx[s->n];
	double dy[s->n];
	double p [s->n];

	for (i = 0 ; i < (s->n-1) ; i++){
		dx[i] = (s->x[i+1]-s->x[i]);
		dy[i] = (s->y[i+1]-s->y[i]);

		p[i] = dy[i]/dx[i];
	}
/*	Build tridiagonal system	*/
	double D[s->n], Q[s->n-1], B[s->n];
	D[0] = 2, Q[0] = 1;
	for (i = 0 ; i < s->n-2 ; i++){
		D[i+1] = 2*dx[i]/dx[i+1] + 2;
		D[s->n-1] = 2;
		
		Q[i+1] = dx[i]/dx[i+1];

		B[i+1] = 3*( p[i] + p[i+1]*dx[i]/dx[i+1]);
	}
/*	Gauss Elimination	*/
	B[0] 	  = 3*p[0];
	B[s->n-1] = 3*p[s->n-2];
	for (i = 1 ; i < s->n ; i++){
		D[i] -= Q[i-1] / D[i-1];
		B[i] -= B[i-1] / D[i-1];
	}
/*	Back-substitution	*/
	s->b[s->n-i] = B[s->n-1] / D[s->n-1];
	for (i = s->n-2 ; i >= 0 ; i--){
		s->b[i] = (B[i] - Q[i] * s->b[i+1]) / D[i];
	}
	for (i = 0 ; i < s->n-1 ; i++){
		s->c[i] = (-2*s->b[i] - s->b[i+1] + 3*p[i])/dx[i];
		s->d[i] = (s->b[i] + s->b[i+1] - 2*p[i]) / (dx[i]*dx[i]);
	}
	return s;

}

/*	Evaluate spline of data	*/
double cubic_spline_evaluate (cubic_spline *s, double z){
	assert(z >= s->x[0] && z <= s->x[s->n-1]);
	int i = binary_search(s->n, s->x, z);

/*	Calculate interpolating spline 	*/
	double dxz = z-s->x[i];

	double spline = s->y[i]	+ s->b[i]*dxz 
					+ s->c[i]*(dxz*dxz)
					+ s->d[i]*(dxz*dxz*dxz);
	return spline;
}

/* evaluates the derivative of the prebuilt spline at point z */
double cubic_spline_derivative(cubic_spline *s, double z){
	assert(z >= s->x[0] && z <= s->x[s->n-1]);
	int i = binary_search(s->n,s->x,z);

	double dxz = z-s->x[i];
	double spline_der = s->b[i]
						+ 2.0*s->c[i] * dxz	
						+ 3.0*s->d[i] * (dxz*dxz);

	return spline_der;
}
 
/* evaluates the integral of the prebuilt spline from x[0] to z */
double cubic_spline_integral(cubic_spline *s, double z){
	assert(z >= s->x[0] && z <= s->x[s->n-1]);
	int i = 0;
	double spline_int = 0, dx;

	for(i = 0; s->x[i+1] < z; i++){
		dx = s->x[i+1] - s->x[i];

		spline_int += s->y[i]*dx 
					+ (1/2)*s->b[i]*(dx*dx) 
					+ (1/3)*s->c[i]*(dx*dx*dx)
					+ (1/4)*s->d[i]*(dx*dx*dx*dx);
	}
	double dxz = (z-s->x[i]);
	spline_int += s->y[i]*dxz 
				+ (1/2)*s->b[i]*(dxz*dxz) 
				+ (1/3)*s->c[i]*(dxz*dxz*dxz)
				+ (1/4)*s->d[i]*(dxz*dxz*dxz*dxz);

	return spline_int;
}

/* Free the allocated memory	*/
void cubic_spline_free (cubic_spline *s){
	
	free (s->x);
	free (s->y);
	free (s->b);
	free (s->c);
	free (s->d);
	free (s);
}
