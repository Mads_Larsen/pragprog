#include "montecarlo.h"


double fun_spherical(gsl_vector*x){
/*	Test spherical funtion
	∫0π  dx/π ∫0π  dy/π ∫0π  dz/π [1-cos(x)cos(y)cos(z)]^-1 = Γ(1/4)4/(4π3) ≈ 
	1.3932039296856768591842462603255	*/
	double cosine = 1;
	for(int i=0; i< x->size; i++){
		cosine *= cos(gsl_vector_get(x,i)); /*	x²+y²+z²+...	*/
	}
/*	1/(1-cos) * 1/pi³	*/
	return pow(1-cosine,-1)*pow(M_PI,-3); 
}


int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n Multi-threading version of 2D Artificial Neural Network using openMP\n");
	fprintf(stderr, "\n Plain Monte Carlo Integration\n");
	fprintf(stderr, "_____________________________________________________________________________\n");

//	srand((unsigned) time(NULL));

	int dim = 3, N=1e6;
	gsl_vector *a = gsl_vector_calloc(dim);
	gsl_vector *b = gsl_vector_alloc(dim);
	double error, result;

/*	Test spherical funtion
	∫0π  dx/π ∫0π  dy/π ∫0π  dz/π [1-cos(x)cos(y)cos(z)]^-1 = Γ(1/4)4/(4π3) ≈ 
	1.3932039296856768591842462603255	*/

	gsl_vector_set_zero(a);
	gsl_vector_set_all(b,M_PI);
	
	double exact_result=pow(tgamma(1.0/4),4)/4.0/(M_PI*M_PI*M_PI);

/*	Measure time spent	*/
	struct timespec start, finish;
	double elapsed;

	clock_gettime(CLOCK_MONOTONIC, &start);
	MonteCarlo_plain(fun_spherical, a, b, N, dim,&result,&error); 
	clock_gettime(CLOCK_MONOTONIC, &finish);
	elapsed = (finish.tv_sec - start.tv_sec);
	elapsed+= (finish.tv_nsec - start.tv_nsec) * 1e-9;


	fprintf(stderr,"\nSpherical integral ∫_0^π dx/π ∫_0^π dy/π ∫_0^π dz/π [1-cos(x)cos(y)cos(z)]^-1 with %i points:\n", N);
	fprintf(stderr, " Calculated Result=%g\n Exact Result= %.20lg\n Estimated Error= %g\n Actual Error= %g\n", 
		result, exact_result, error, fabs(result-exact_result));

	fprintf(stderr, "Time used = %g\n", elapsed);

	gsl_vector_free(a); gsl_vector_free(b);

	return EXIT_SUCCESS;
}