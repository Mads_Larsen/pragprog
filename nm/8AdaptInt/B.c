#include "header.h"

int main(){
	fprintf(stderr, "\n____________________________________________________\n");
	fprintf(stderr, "\n B. Infinite Limits\n");
	fprintf(stderr, "____________________________________________________\n");

	int evals = 0;
	double Q, a, b, err, acc=0.000001, eps=0.000001;
	double func_exp (double x){
		double r= exp(-pow(x,2));
		return r;
	};				/* = 2/3	*/

	fprintf(stderr, "Infinite Integral of exp(-x²)\n");
	fprintf(stderr, "limit\t calculated\t exact\t evals\t error\n");

	a = -INFINITY; b = INFINITY;	
	Q = integrator_limits(func_exp, a, b, acc, eps, &err);
	fprintf(stderr, "-∞,∞\t %g\t √π\t %i\t %g\n", Q, evals, err);

	a = 0; b = INFINITY;	
	Q = integrator_limits(func_exp, a, b, acc, eps, &err);
	fprintf(stderr, "0,∞\t %g\t √π/2\t %i\t %g\n", Q, evals, err);
	
	a = 0; b = INFINITY;	
	Q = integrator_limits(func_exp, a, b, acc, eps, &err);
	fprintf(stderr, "-∞,0\t %g\t √π/2\t %i\t %g\n", Q, evals, err);

	a = 0; b = 1;	
	Q = integrator_limits(func_exp, a, b, acc, eps, &err);
	fprintf(stderr, "0,1\t %g\t 0.746824\t %i\t %g\n", Q, evals, err);

	return EXIT_SUCCESS;
}
