#include <stdio.h>
#include <limits.h>
#include <float.h>

void precission();
void sum();
int equal(double a, double b, double tau, double epsilon);
void name_digit(int c);

int main(){

	printf("\nExercise Epsilon\n");

	precission();
	sum();
	equal(1, 1.2, 0.1, 0.1);
	name_digit(3);
	
	return 0;
}