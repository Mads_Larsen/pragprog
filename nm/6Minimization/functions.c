#include "header.h"
double Rosenbrock(gsl_vector* p, gsl_vector* gradient){
/*	f(x,y) = (1-x)^2+100(y-x^2)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	Gradient	*/
	gsl_vector_set(gradient,0, 2* ((-1) + x + 200 * x*x*x - 200 * x * y));/* dfdx */
	gsl_vector_set(gradient,1, 200 *(y-x*x) );							/* dfdy	*/

	double Rosenbrock = ( (1-x)*(1-x) ) + 100 * ( (y-(x*x))*(y-(x*x)) );
	return Rosenbrock;
}

void Rosenbrock_hessian(gsl_vector * p, gsl_matrix * H){
/*	f(x,y) = (1-x)^2+100(y-x^2)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);

	/*	Hessian Matrix 	*/
	gsl_matrix_set(H,0,0, 2 + 1200 * x*x - 400 * y );	/* d²f/dx²	*/
	gsl_matrix_set(H,1,1, 200);							/* d²f/dy²	*/
	gsl_matrix_set(H,0,1, -400*x); /* d/dy(df/dx) = d/dx(df/dy) */
	gsl_matrix_set(H,1,0, -400*x); /* -> Symmetric Matrix 		*/
/*  if the second derivatives of f are all continuous,
	then the Hessian of f is a symmetric matrix */
}

double Himmelblau(gsl_vector* p, gsl_vector* gradient){
/*	f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
/*	Gradient	*/
//	gsl_vector_set(gradient,0, 2 *(-7 + x + y*y + 2*x *(-11 + x*x + y)) );/* dfdx */
//	gsl_vector_set(gradient,1, 2 *(-11 + x*x + y + 2*y *(-7 + x + y*y)) );/* dfdy	*/
	gsl_vector_set(gradient,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7));/* dfdx */
	gsl_vector_set(gradient,1, 2*(x*x+y-11)    +2*(x+y*y-7)*2*y);/* dfdy	*/

	double Himmelblau = (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);
	return Himmelblau;
}

void Himmelblau_hessian(gsl_vector * p, gsl_matrix * H){
/*	f(x,y) = (x^2+y-11)^2+(x+y^2-7)^2	*/
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);

	/*	Hessian Matrix 	*/
	gsl_matrix_set(H,0,0, -42 + 12*x*x + 4*y );	/* d²f/dx²	*/
	gsl_matrix_set(H,1,1, -26 + 4*x + 12*y*y );	/* d²f/dy²	*/
	gsl_matrix_set(H,0,1, 4*(x+y) ); /* d/dy(df/dx) = d/dx(df/dy) */
	gsl_matrix_set(H,1,0, 4*(x+y) ); /* -> Symmetric Matrix 		*/	
}

double expo_decay(double t, gsl_vector *p){
	double A = gsl_vector_get(p,0);
	double T = gsl_vector_get(p,1);
	double B = gsl_vector_get(p,2);

	return A*exp(-t/T)+B;
}

double expo_decay_min(gsl_vector *p, gsl_vector* gradient) {
	double A = gsl_vector_get(p,0);
	double T = gsl_vector_get(p,1);
	double B = gsl_vector_get(p,2);

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int n = sizeof(t)/sizeof(t[0]);


	double master_func =0, grad1 = 0, grad2 = 0, grad3 = 0, temp;
	for( int i = 0; i < n; i++) {
		temp = 2*(A*exp(-t[i]/T)+B-y[i])/pow(e[i],2);
		grad1 += exp(-t[i]/T)*temp;
		grad2 += A*t[i]/pow(T,2)*exp(-t[i]/T)*temp;
		grad3 += temp;
	/*	The (master) function to minimize is F(A,T,B)=∑i(f(ti)-yi)²/σi² */
		master_func += pow( (A*exp(-t[i]/T)+B-y[i])/e[i] , 2 );
	}
	gsl_vector_set(gradient,0,grad1);
	gsl_vector_set(gradient,1,grad2);
	gsl_vector_set(gradient,2,grad3);

	return master_func;
}
