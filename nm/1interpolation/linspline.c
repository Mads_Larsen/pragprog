#include "interpolation.h" 

double linear_interpolation(int dim, double *x, double *y, double z){
	assert(dim > 1 && z >= x[0] && z <= x[dim-1]);

/*	Binary Search of the interval [xi ≤ x ≤ xi+1] */
	int i = binary_search(dim,x,z);

/*	Linear polynomial	*/
	double p = ( y[i+1]-y[i]) / (x[i+1]-x[i]);
	double S = y[i] + p * (z-x[i]);

	return S;
}

double linear_interpolation_integral(
	int dim, double *x, double *y, double z){
	assert(dim > 1 && z >= x[0] && z <= x[dim-1]);

	int i;
	double Sint=0, p;

/*	Sumarize from x[0] to x[i+1]<z over Linear polynomial 
	Integrated from x[i] to x[i+1] 	*/
	for(i = 0; x[i+1] < z; i++){

		p = ( y[i+1]-y[i]) / (x[i+1]-x[i]);
		Sint += y[i] * (x[i+1]-x[i]) + p*((x[i+1]*x[i+1]-x[i]*x[i])/2 
					- x[i]*x[i+1] + x[i]*x[i]);
	}

/*	Add last value of Spline intraged from x[i] to z-x*/
	p = ( y[i+1]-y[i]) / (x[i+1]-x[i]);
	Sint += y[i] * (z-x[i]) + p*((z*z-x[i]*x[i])/2 
					- x[i]*z + x[i]*x[i]);
	return Sint;
}