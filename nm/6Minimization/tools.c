#include "header.h"

/*
void matrix_print(const char* s,const gsl_matrix* m){	// Prints matrix
	fprintf(stderr,"\n%s\n",s);
	for(int j=0;j<m->size1;j++){	// '->' dereferences m - size2 = column
		for(int i=0;i<m->size2;i++)	//	size1 = row
			fprintf(stderr,"%8.3f ",gsl_matrix_get(m,j,i));
		fprintf(stderr,"\n");
	}
}

void vector_print(const char* s,const gsl_vector* v){	// Prints vector
	fprintf(stderr,"\n%s\n",s);
	for(int i=0; i<v->size; i++){	// '->' dereferences m - size = row
			fprintf(stderr,"%8.3f ",gsl_vector_get(v,i));
		fprintf(stderr,"\n");
	}
}

*/
double vector_dot_prod(gsl_vector * x, gsl_vector * y){
	int n = x->size;
	double product=0;
	for (int i = 0; i<n; i++){
		product += gsl_vector_get(x,i)*gsl_vector_get(y,i);
	}
	return product;
}

void backtracking_linesearch(
	double f(gsl_vector* p, gsl_vector* gradient),
	gsl_vector * x, gsl_vector * Dx){
	/* 	backtracking line search where one ﬁrst attempts the full step,
		λ = 1, and then backtracks, λ ← λ/2	*/

	int n = x->size;

	gsl_vector* gradient = gsl_vector_alloc(n);
	gsl_vector* gradientz = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);

	double fx = f(x,gradient), fz;
	double step_modulator=1, step_modulator_min = 0.02; /* Stepsize modulator, lambda */
	while(1){
		//fprintf(stderr, "iter = %i\n", iter);
		gsl_vector_memcpy(z,x); /* If step didn't work -> Go back to old x and try with new stepsize modulator */
		gsl_vector_add(z,Dx);

		fz = f(z,gradientz); /* The redefined H is not used... */

	/*	Stop loop if Armijo condition is met, or λ<λ_min
		f(x + λ∆x) < f(x) + αλ∆xT∇f(x)  */
//		fprintf(stderr, "fz=%g fx+bla = %g\n", fz,fx+vector_dot_prod(Dx,gradientz));
		if ( fz < fx + 1e-4*step_modulator*vector_dot_prod(Dx,gradient) 
			|| step_modulator < step_modulator_min ) break;

		step_modulator*=0.5; /* λ ← λ/2 */
		gsl_vector_scale(Dx,0.5);
		}
/*	Copy x values back 	*/
	gsl_vector_memcpy(x,z);

/*	Free allocated Memory 	*/
	gsl_vector_free(gradient); gsl_vector_free(gradientz);
	gsl_vector_free(z);
}

void backtracking_linesearch_qn(
	double f(gsl_vector* p, gsl_vector* gradient),
	gsl_matrix * H, gsl_vector * x, gsl_vector * Dx){
	/* 	backtracking line search where one ﬁrst attempts the full step,
		λ = 1, and then backtracks, λ ← λ/2	*/

	int n = x->size;

	gsl_vector* gradient = gsl_vector_alloc(n);
	gsl_vector* gradientz = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);

	double fx = f(x,gradient), fz;
	double step_modulator=1, step_modulator_min = 0.002; /* Stepsize modulator, lambda */
	while(1){
		//fprintf(stderr, "iter = %i\n", iter);
		gsl_vector_memcpy(z,x); /* If step didn't work -> Go back to old x and try with new stepsize modulator */
		gsl_vector_add(z,Dx);

		fz = f(z,gradientz); /* The redefined H is not used... */

	/*	Stop loop if Armijo condition is met, or λ<λ_min
		f(x + λ∆x) < f(x) + αλ∆xT∇f(x)  */
//		fprintf(stderr, "fz=%g fx+bla = %g\n", fz,fx+vector_dot_prod(Dx,gradientz));
		if ( fz < fx + 1e-4*step_modulator*vector_dot_prod(Dx,gradient) ) break;
		if (step_modulator < step_modulator_min ){
		/*	If the update diverges, reset the inverse Hessian matrix to unity and continue. */
			gsl_matrix_set_identity(H);
			break; 
		}

		step_modulator*=0.5; /* λ ← λ/2 */
		gsl_vector_scale(Dx,0.5);
		}
/*	Copy x values back 	*/
	gsl_vector_memcpy(x,z);

/*	Free allocated Memory 	*/
	gsl_vector_free(gradient); gsl_vector_free(gradientz);
	gsl_vector_free(z);
}