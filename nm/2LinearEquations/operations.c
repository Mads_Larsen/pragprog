#include "lineq.h"

void gs_decomp(gsl_matrix * A, gsl_matrix * Q, gsl_matrix * R){
	int n = A->size1, m = A->size2; /* n=row, m=column */
	int i=0, j=0, l = 0;
	for (i=0 ; i<m ; i++){
		gsl_matrix_set(R, i,i, sqrt(matrix_col_inner_prod(A,i,A,i)) );
	
		/* Q from q_i */
		for (l = 0 ; l<n ; l++){
			gsl_matrix_set(Q,l,i, 
				gsl_matrix_get(A,l,i) / gsl_matrix_get(R,i,i));
		}

		for (j = i+1 ; j<m ; j++){
			/* R_ij */
			gsl_matrix_set(R,i,j,
				matrix_col_inner_prod(Q,i,A,j)
			);
			/* a_j */
			for (l = 0 ; l<n ; l++){
				gsl_matrix_set(A,l,j,
					gsl_matrix_get(A,l,j)
					- gsl_matrix_get(Q,l,i)*gsl_matrix_get(R,i,j)
				);
			}
		}
	}
}

void gs_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x){
/*	Solve Rx = QTb	*/
/*	calculate Q^T*b = c */
	int m = R->size1;
	gsl_vector *c = gsl_vector_alloc(m);
	gsl_blas_dgemv(CblasTrans,1, Q, b, 0, c);
	
	int n = c->size-1; /* Last index */
/*	Back Substitute	*/
	for (int i = n ; i>=0 ; i--){
		double s = gsl_vector_get(c,i);
		for (int k=i+1 ; k < m ; k++){
			s-=gsl_matrix_get(R,i,k) * gsl_vector_get(x,k);
		}
		gsl_vector_set(x,i,
				s/gsl_matrix_get(R,i,i)
			);
	}
	gsl_vector_free(c);
}

void gs_inverse(gsl_matrix *Q, gsl_matrix *R, gsl_matrix *B){
	int n = Q->size1;
	gsl_vector *e = gsl_vector_calloc(n); /* Unit vector */
	gsl_vector *x = gsl_vector_calloc(n); /* Result of backsub, solve */

	for (int i=0 ; i<n ; i++){
		gsl_vector_set(e,i,1.0);	/* 	Set to 1 for current iteration	*/
		gs_solve(Q,R,e,x); 			/*	Backsubstitute	*/
		gsl_vector_set(e,i,0.0); 	/* 	set to 0 for next iteration	*/
		gsl_matrix_set_col(B,i,x);
	}
/*	vector e and x are no longer needed	*/
	gsl_vector_free(e);	gsl_vector_free(x);
}