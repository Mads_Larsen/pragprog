
/* 	Minimum of the rosenbrock function
	Find, using one of the minimization routines from GSL, the
	minimum of the Rosenbrock function
	f(x,y) = (1-x)² + 100(y-x²)²  */
#include "stdio.h"
#include "stdlib.h"
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_vector.h>

double rosenbrock(const gsl_vector * xy, void *params){

	double x = gsl_vector_get(xy, 0);
	double y = gsl_vector_get(xy, 1);

	double f = ( (1-x)*(1-x) ) + 100 * ( (y-(x*x))*(y-(x*x)) );

	return f;
}

int main(void){
/*	initialize minimizer state, s, for algorithm T
	update s using the iteration T
	test s for convergence, and repeat iteration if necessary */

	int iter = 0, dim = 2, status;
	double size;

/*	Define function	*/
	gsl_multimin_function func;
	func.f = &rosenbrock;
	func.n = dim;
	func.params = NULL;

/*	Intialize solver 	*/
	const gsl_multimin_fminimizer_type *T =
		gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *s = 
		gsl_multimin_fminimizer_alloc(T,dim);

/*	Starting point */
	gsl_vector *x = gsl_vector_alloc(dim);
	gsl_vector_set (x,0,0);
	gsl_vector_set (x,1,0);

/*	Initial step size	*/
	gsl_vector *stepsize = gsl_vector_alloc(dim);
	gsl_vector_set_all (stepsize, 1.0);

/*	Iterate	*/
	gsl_multimin_fminimizer_set (s, &func, x, stepsize);

	fprintf(stdout, "iter\tx\ty\tf\tsize\n");
	do
    {
	iter++;
	status = gsl_multimin_fminimizer_iterate(s);

	if (status)
	break;

	size = gsl_multimin_fminimizer_size (s);
	status = gsl_multimin_test_size (size, 1e-2);

/*	if (status == GSL_SUCCESS)
	{
	printf ("converged to minimum at\n");
	}
*/
	fprintf (stdout, "%5d\t%10.3e\t%10.3e\t%7.3f\t%.3f\n",
	  iter,
	  gsl_vector_get (s->x, 0),
	  gsl_vector_get (s->x, 1),
	  s->fval, size);
    }
  	while (status == GSL_CONTINUE && iter < 100);

/*	To rosenbrock_min.txt */
  	fprintf(stderr, "\n1. Minimum of Rosenbrock function\n");
  	fprintf(stderr, "Rosenbrock function converged at:\n");
  	fprintf(stderr, "iter = %i\tx = %g\ty = %g\tf() = %g\tsize = %g\n",
  		iter, gsl_vector_get(s->x,0), gsl_vector_get(s->x,1),
  		s->fval, size);

/* 	Free allocatd memory */
	gsl_vector_free (x);
	gsl_vector_free (stepsize);
	gsl_multimin_fminimizer_free (s);

	return EXIT_SUCCESS;
}