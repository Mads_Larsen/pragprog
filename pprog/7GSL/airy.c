#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_sf_airy.h> // Includes airy functions
#define mode GSL_PREC_APPROX // GSL_PREC_DOUBLE for higher precision

int main()
{
	double x;
	printf("x \t Ai \t Bi \n");	// Tab separated column names
	while(scanf("%lg",&x)!=EOF){

	double Ai = gsl_sf_airy_Ai(x, mode);
	
	double Bi = gsl_sf_airy_Bi(x, mode);

	printf("%g \t %g \t %g \n", x,Ai,Bi);} // Tan separated data columns
	return 0;
}