#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h> // For GSL_SUCCESS 
#include <gsl/gsl_odeiv2.h>
#include <getopt.h>
#define steptype gsl_odeiv2_step_rk8pd // #define steptype gsl_odeiv2_step_rkf45


int errfunc( double x, const double u[], double f[], void * params){
	
	//(void)(x);	// Avoid unused parameter warning
	f[0] = u[1];
	f[0] = ( 2 / (sqrt(M_PI))) * exp(- (x*x) );

	return GSL_SUCCESS;
}

int main(int argc, char const *argv[])
{
	double x_initial = atof (argv[1]);
	double x_max = atof (argv[2]);
	double x_step = atof (argv[3]);
	
	int dim = 1;
	gsl_odeiv2_system sys;
	sys.function = errfunc;
	sys.jacobian = NULL;
	sys.dimension = dim;
	sys.params = NULL;

	
/*	Initial conditions	*/
	double x=0;
	double u[1] = {0};


/*	Iterate	*/
	double accuracy = 1e-6, eps = 1e-6, hstart;

/*	Solves negative initial x from initial u'=0	*/
	hstart = copysign(1e-3,x_initial);
	gsl_odeiv2_driver * driver = gsl_odeiv2_driver_alloc_y_new (
		&sys, steptype, hstart, accuracy, eps);
	gsl_odeiv2_driver_apply (driver, &x, x_initial, u);

/*	Solves from x_init to x_max	*/
	hstart = 1e-3;
	driver = gsl_odeiv2_driver_alloc_y_new (
							&sys, steptype, hstart, accuracy, eps);
	printf("x\tu\ttheory\n");
	for (x = x_initial; x <= x_max; x += x_step){
		int status = gsl_odeiv2_driver_apply (driver, &x_initial, x, u);
		printf("%g\t%g\t%g\n", x, u[0], erf(x) );
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
	}


	gsl_odeiv2_driver_free (driver);	// Free allocated driver

	return EXIT_SUCCESS;
}
