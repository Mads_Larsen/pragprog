#include "header.h"

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n B. Check that the error of the plain Monte-Carlo method behaves as O(1/√N). \n");
	fprintf(stderr, "_____________________________________________________________________________\n");

	srand((unsigned) time(NULL));

	int dim = 3, N = 1e3;
	double result, error;
	gsl_vector*a = gsl_vector_alloc(dim);
	gsl_vector*b = gsl_vector_alloc(dim);

	gsl_vector_set_zero(a);
	gsl_vector_set_all(b,1);
	fprintf(stderr, "Testing order of square function x²+y²+z² from x,y,z=0 to 1\n");
	fprintf(stderr, "See figure B.svg\n");
//	fprintf(stdout, "N\tError\n");
	for(int n = 10; n<N; n++){
		MonteCarlo_plain(fun_square, a, b, n, dim,&result,&error);
		fprintf(stdout, "%i\t%g\n",
			n, error);
	}
	fprintf(stdout, "\n\n");
	return EXIT_SUCCESS;
}