#include "lineq.h"

int main () {
	fprintf(stderr, "\nLinear Equations\n");
/*	A Solving linear equations using QR-decomposition 
	by modified Gram-Schmidt orthogonalization	*/
	fprintf(stderr, " A. QR-decomposition by GS orthogonalization\n");
	fprintf(stderr, " 1. Decomposition of a tall matrix\n");
/*	Define Tall matrix n*m for n<m 	*/
	int n=4,m=3;
	gsl_matrix * A1 = gsl_matrix_alloc(n,m);
	matrix_fill(A1);
	matrix_print("Tall matrix A1", A1);

/*	Allocate Q and R. R is a zero matrix */
	gsl_matrix * Q1 = gsl_matrix_alloc(A1->size1, A1->size2);
	gsl_matrix * R1 = gsl_matrix_calloc(A1->size2, A1->size2); 
/*	Q and R are changed by the function. A is changed too.	*/
	gs_decomp(A1,Q1,R1);

	matrix_print("Q1 matrix", Q1);
	matrix_print("R1 matrix", R1);

	gsl_matrix * QQ = gsl_matrix_alloc(A1->size2, A1->size2);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, Q1, Q1, 0, QQ);
	matrix_print("Q1 transposed * Q1 = should be identity", QQ);

	gsl_matrix * QR = gsl_matrix_alloc(A1->size1, A1->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, Q1, R1, 0, QR);	
	matrix_print("Q1 * R1 = Should be A1", QR);

/* 	A.2 Implement a function that given the matrices Q and R the 
	equation QRx=b by applying QT to the vector b (saving the result in x) 
	and then performing in-place back-substitution on x. */
	fprintf(stderr, "\n 2. Solve equation QRx=b \n");
/*	Define square matrix */
	gsl_matrix * A2 = gsl_matrix_alloc(m,m);
	gsl_matrix * A2_copy = gsl_matrix_alloc(m,m); /* To be destroyed */
	matrix_fill(A2);
	gsl_matrix_memcpy(A2_copy, A2);
	matrix_print("Square Matrix A2\n", A2);

/*	Define vector of same size */
	gsl_vector * b2 = gsl_vector_alloc(m);
	gsl_vector * x = gsl_vector_alloc(m);
	vector_fill(b2);
	fprintf(stderr, "Vector b2 \n");
	gsl_vector_fprintf(stderr,b2,"%g");

/*	Factorize A2 into Q2R2	*/
/*	Allocate Q2 and R2. R2 is a zero matrix */
	gsl_matrix * Q2 = gsl_matrix_alloc(A2->size1, A2->size2);
	gsl_matrix * R2 = gsl_matrix_calloc(A2->size2, A2->size2);
	gs_decomp(A2_copy,Q2,R2);
	matrix_print("Q2 matrix", Q2);
	matrix_print("R2 matrix", R2);

	gs_solve(Q2,R2,b2,x);
	fprintf(stderr, "x vector\n");
	gsl_vector_fprintf(stderr,x,"%g");

	gsl_vector *A2x = gsl_vector_alloc(m);
	gsl_blas_dgemv(CblasNoTrans,1, A2, x, 0, A2x);
	fprintf(stderr, "\nA2*x = should be b2\n");
	gsl_vector_fprintf(stderr,A2x, "%g");



	gsl_matrix_free(A1); gsl_matrix_free(A2);	gsl_matrix_free(A2_copy);
	gsl_vector_free(b2);
	gsl_vector_free(x);
	gsl_matrix_free(Q1); gsl_matrix_free(Q2);
	gsl_matrix_free(R1); gsl_matrix_free(R2);
	gsl_matrix_free(QQ);
	gsl_matrix_free(QR);
	return EXIT_SUCCESS;
}
