#include "exam.h"

int adaptive_driver(
	double* t,                             /* the current value of the variable */
	double b,                              /* the end-point of the integration */
	double* h,                             /* the initial step-size */
	gsl_vector*ynow,                         /* the current y(t) */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double tprev, double t, double h, 
		gsl_vector*yprev, gsl_vector*ynow,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*ynew, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt), /* right-hand-side */
	gsl_vector* err
){
	int n = ynow->size, iter=0;
	double tolerance, tprev;
	gsl_vector* yprev = gsl_vector_alloc(n);
	gsl_vector* ynew = gsl_vector_alloc(n);

/*	Generate first value using Runga-Kutta12 outside loop	*/
		if (fabs(*t + *h) > fabs(b)) *h = b - *t;
		rkstep12(*t,*h, ynow,f,ynew,err);
		tolerance = (acc+eps*gsl_blas_dnrm2(ynew)); 	/*	tolerance = acc + eps * |ynew|²	*/
		if(tolerance > gsl_blas_dnrm2(err)){
		/*	copy t->tprev, yi->yi-1 and yi+1->i */
			tprev=*t;
			*t += *h;
			gsl_vector_memcpy(yprev, ynow);
			gsl_vector_memcpy(ynow, ynew);
			iter++;
		}
		*h = *h*pow(tolerance/gsl_blas_dnrm2(err),0.25)*0.95; 	/*	h_i+1 = h_i × (τi /  ei)^Power × Safety	 */
/*	Iterate using two-step method	*/
	do{
		if (fabs(*t + *h) > fabs(b)) *h = b - *t;
		if(*t>=b) break;
		stepper(tprev,*t,*h,yprev, ynow,f,ynew,err);
		tolerance = (acc+eps*gsl_blas_dnrm2(ynew)); 	/*	tolerance = acc + eps * |ynew|²	*/
		if(tolerance > gsl_blas_dnrm2(err)){
		/*	copy t->tprev, yi->yi-1 and yi+1->i */
			tprev=*t;
			*t += *h;
			gsl_vector_memcpy(yprev, ynow);
			gsl_vector_memcpy(ynow, ynew);
			iter++;
		}
		*h = *h*pow(tolerance/gsl_blas_dnrm2(err),0.25)*0.95; 	/*	h_i+1 = h_i × (τi /  ei)^Power × Safety	 */
	}while(iter < 1e6 );

	gsl_vector_free(yprev); gsl_vector_free(ynew);

	return iter;
}

int adaptive_driver_path(
	gsl_matrix* path,
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double tprev, double t, double h, 
		gsl_vector*yprev, gsl_vector*ynow,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*ynew, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
){
/*	Define things	*/
	int n = path->size2-1, max_steps = path->size1, step = 0;
	double tolerance, t, tprev;
    gsl_vector* yprev = gsl_vector_calloc(n);
    gsl_vector* ynow = gsl_vector_calloc(n);
	gsl_vector* ynew = gsl_vector_calloc(n);
    gsl_vector* err = gsl_vector_calloc(n);

/*	Generate first value using Runga-Kutta12 outside loop	*/
	t = gsl_matrix_get(path,step,0);
	for(int i=0; i<n; i++){
		gsl_vector_set(ynow,i,  gsl_matrix_get(path,step  , i+1));
	}
	if (fabs(t + *h) > fabs(b)) *h = b - t;
	rkstep12(t,*h, ynow,f,ynew,err);
	tolerance = (acc+eps*gsl_blas_dnrm2(ynew)); 	/*	tolerance = acc + eps * |ynew|²	*/
	if(tolerance > gsl_blas_dnrm2(err)){
	/*	Set t and y values in path matrix 	*/
		step++;
		gsl_matrix_set(path, step, 0, t+ *h);
		for(int i=0; i<n; i++){
			gsl_matrix_set(path, step, i+1, gsl_vector_get(ynew,i));
		}

	}
		*h = *h*pow(tolerance/gsl_blas_dnrm2(err),0.25)*0.95; 	/*	h_i+1 = h_i × (τi /  ei)^Power × Safety	 */
			
    /*	While t_i < b 	*/
	while(gsl_matrix_get(path,step,0) < b){
	/*	Set parameters t and y	*/
		tprev = gsl_matrix_get(path,step-1,0);	
		t = gsl_matrix_get(path,step,0);
		for(int i=0; i<n; i++){
		gsl_vector_set(yprev,i, gsl_matrix_get(path,step-1, i+1));
		gsl_vector_set(ynow,i,  gsl_matrix_get(path,step  , i+1));
		}
		
		if (fabs(t + *h) > fabs(b)) *h = b - t;
		if(t>=b) break;
		
		stepper(tprev,t,*h,yprev, ynow,f,ynew,err);
		tolerance = (acc+eps*gsl_blas_dnrm2(ynew));
		
		if(tolerance > gsl_blas_dnrm2(err)){
			step++;
			if(step+1 > max_steps){
				fprintf(stderr, "\n Max number of steps \n");
				break;
			}
		/*	Set t and y values in path matrix 	*/
			gsl_matrix_set(path, step, 0, t+ *h);
			for(int i=0; i<n; i++){
				gsl_matrix_set(path, step, i+1, gsl_vector_get(ynew,i));
			}	
		}
		if( err > 0){
			*h = *h*pow(tolerance/gsl_blas_dnrm2(err),0.25)*0.95;
		}
		else *h=2;
	}

	gsl_vector_free(ynew); gsl_vector_free(err);

	return step;
}


/*	Runga-Kutta 12 method for finding the first step	*/
void rkstep12(
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* ynow, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* ynew,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
){

	int n = ynow->size;
/*	Allocate memory	*/
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);
	
	gsl_vector_memcpy(ynew,ynow);

	f(t,ynow,k0);
/*	daxpy: y = ax + y 	*/
	gsl_blas_daxpy(h/2, k0,  ynow);	/* y_i+1 = y_i + k0_i * h/2 */
	f(t+h/2, ynow, k12);
	gsl_blas_daxpy(h, k12, ynew);	/* y_i+1 + h[i] = y_i + k12_i * h */

/*	err[i] = (k0[i] - k12[i]) * h/2;	*/
	gsl_vector_sub(k0,k12);
	gsl_vector_scale(k0,h/2);
	gsl_vector_memcpy(err,k0);

/*	Free Allocated Memory 	*/
	gsl_vector_free(k0); gsl_vector_free(k12);
}