#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <gsl/gsl_rng.h>

/*	Functions	*/												
/*	Operations	*/
void randomx(int dim, gsl_vector*a, gsl_vector*b, gsl_vector*x, gsl_rng *RNG);
void MonteCarlo_plain(
	double f(gsl_vector*x),
	gsl_vector*a, gsl_vector*b, 
	int N, int dim,
	double*result, double*error);

/* 	Tools	*/
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void vector_print(const char* s,const gsl_vector* v);	// Prints vector

#define HAVE_HEADER_H
#endif