#include "interpolation.h"
#include <math.h>
#include <gsl/gsl_spline.h>
#define RND (double)rand()/RAND_MAX

int main(void)
{
	fprintf(stderr, "\nNumerical Methods\nExercise: Interpolation\n\n");
	fprintf(stderr, "See figures for linear, quadratic & cubic splines,\n");
	fprintf(stderr, "as well as a comparison of the 3 interpolation types.\n\n");

/*	Define variables	*/
	int dim = 30, i;

/*	Define z. Points of spline function	*/
	double 	z=0.0, dz = 0.1;


/*	Define x, y points to be splined.
	Using cosine to have something to compare with	*/
/*	Allocate memory for x and y	*/
	double x[dim], y[dim]; 

/*	Generate x and y values, and print to stdout	*/
	fprintf(stdout, "iter\tx\ty\n");
	for (i = 0; i < dim; i++){
		x[i] = i/M_PI;
		y[i] = cos(x[i]);
		fprintf(stdout, "%3i\t%8g\t%8g\n",
			i, x[i], y[i] );
	}
	fprintf(stdout, "\n\n");	// New data in stdout


/*	Linear interpolation
	Generate z splined values, S, and 
	print with math cos for comparison */
	double linspline, linspline_int;
	fprintf(stdout, "z\tS\tSint\tmath_cos\n");
	for (z = x[0]; z <= x[dim-1] ; z += dz){

		linspline 		= linear_interpolation 		  	(dim, x, y, z);
		linspline_int 	= linear_interpolation_integral	(dim, x,y,z);

		fprintf(stdout, "%.4g\t%.4g\t%.4g\t%.4g\n",
			z, linspline, linspline_int, cos(z));
	}
	fprintf(stdout, "\n\n");

/*	Quadratic Interpolation 	*/
	qspline *qs = qspline_alloc(dim);
	qs = qspline_coeff(qs, x,y);

	double	quadspline, quadspline_int,	quadspline_der;
	fprintf(stdout, "z\tqs\tqs_der\tqs_int\n");
	for (z = x[0]; z <= x[dim-1] ; z += dz){
		
		quadspline 		= qspline_evaluate		(qs,z);
		quadspline_der	= qspline_derivative	(qs,z);
		quadspline_int	= qspline_integral		(qs,z);

		fprintf(stdout, "%.4g\t%.4g\t%.4g\t%.4g\n",
			z,quadspline, quadspline_der, quadspline_int);
	}
	fprintf(stdout, "\n\n");

/*	Cubic Interpolation 	*/
	cubic_spline *cs = cubic_spline_alloc(dim);
	cs = cubic_spline_coeff(cs, x,y);

	gsl_interp_accel *acc 	= gsl_interp_accel_alloc ();
    gsl_spline *gsl_spline 	= gsl_spline_alloc (gsl_interp_cspline, dim);
    gsl_spline_init (gsl_spline, x, y, dim);

	double	cubicspline, cubicspline_int,
			cubicspline_der, gsl_z;

	fprintf(stdout, "z\tcs\tcs_der\tcs_int\tgsl_z\n");
	for (z = x[0]; z <= x[dim-1] ; z += dz){

		cubicspline 	= cubic_spline_evaluate		(cs, z);
		cubicspline_der	= cubic_spline_derivative	(cs, z);
		cubicspline_int	= cubic_spline_integral		(cs, z);

		gsl_z = gsl_spline_eval (gsl_spline, z, acc);

		fprintf(stdout, "%.4g\t%.4g\t%.4g\t%.4g\t%.4g\n",
			z,cubicspline, cubicspline_der, cubicspline_int, gsl_z);
	}

	fprintf(stdout, "\n\n");

/*	Comparison between splines 	
	Using randomly generated data*/
	dim = 10;

	qs = qspline_alloc(dim);
	cs = cubic_spline_alloc(dim);

	double xRND[dim], yRND[dim];

	fprintf(stdout, "xRND\tyRND\n");
	for(i=0 ; i < dim ; i++){
		xRND[i]=0.0+i;
		yRND[i]=0.2*i+RND;
		printf("%.4g\t%.4g\n",xRND[i],yRND[i]);
	}
	printf("\n\n");

/*	Spline evaluations	*/
	qs = qspline_coeff(qs, xRND,yRND);
	cs = cubic_spline_coeff(cs, xRND,yRND);
	double 	zRND, linsplineRND,	quadsplineRND, cubicsplineRND;

	fprintf(stdout, "zRND\tlsRND\tqsRND\tcsRND\n");
	for (zRND = x[0]; zRND <= xRND[dim-1] ; zRND += dz){

		linsplineRND 	= linear_interpolation 	(dim, xRND, yRND, zRND);
		quadsplineRND 	= qspline_evaluate 		(qs, zRND);
		cubicsplineRND	= cubic_spline_evaluate (cs, zRND);

		fprintf(stdout, "%.4g\t%.4g\t%.4g\t%.4g\n",
			zRND,
			linsplineRND, quadsplineRND, cubicsplineRND );
	}

/* 	Free allocated memory	*/
    gsl_spline_free (gsl_spline);
    gsl_interp_accel_free (acc);
	qspline_free(qs);
	cubic_spline_free (cs);

	return 0;
}