#ifndef HAVE_HEADER_H /* For multiple includes	*/

/* Libraries	*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/*	Functions	*/
/*	Operations	*/
void rkstep12(
	double t,											/* the current value of the variable */
	double h,											/* the step to be taken */
	gsl_vector* yx, 									/* the current value y(t) of the sought function */
	void f(double t, gsl_vector* y, gsl_vector* dydt),	/* the right-hand-side, dydt = f(t,y) */
	gsl_vector* yth,									/* output: y(t+h) */
	gsl_vector* err 									/* output: error estimate dy */
);

void driver(
	double* t,                             /* the current value of the variable */
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	gsl_vector*yt,                         /* the current y(t) */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err
		),
	void f(double t,gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
);

int driver_path(
	gsl_matrix* path,
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector*y,gsl_vector*dydt),
		gsl_vector*yth, gsl_vector*err
		),
	void f(double t, gsl_vector*y,gsl_vector*dydt) /* right-hand-side */
);
/* 	Tools	*/
void matrix_print(const char* s,const gsl_matrix* m);	// Prints matrix
void vector_print(const char* s,const gsl_vector* v);	// Prints vector

#define HAVE_HEADER_H
#endif