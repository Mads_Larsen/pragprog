
#include "header.h"

void least_squares_fit(int m, double func(int i, double xi), 
	gsl_vector * x, gsl_vector * y, gsl_vector * dy,	/*	Data */
	gsl_vector * c, gsl_vector * c_error, gsl_matrix * covariance){
/* 	We want to solve Rc = Q^T b, minimizing ||Rc-Q^Tb||^2
	Factorizing an n*m matrix A=QR.
	Where A_ik = f_k(x_i) / dy_i and b_i = y_i / dy_i	*/

/* 	Allocate matrices	*/
	int n = x->size;
	gsl_matrix *A    = gsl_matrix_alloc(n,m);
	gsl_vector *b    = gsl_vector_alloc(n);
	gsl_matrix *Q    = gsl_matrix_alloc(n,m);
	gsl_matrix *R    = gsl_matrix_alloc(m,m);
	gsl_matrix* invR = gsl_matrix_alloc(m,m);
	gsl_matrix *I    = gsl_matrix_alloc(m,m);
	gsl_matrix_set_identity(I);

/*	Set A_ik and b_ik	*/
	for (int i=0; i<n; i++){ /* row */
	/*	retrieve i'th value */
		double xi = gsl_vector_get(x,i);
		double yi = gsl_vector_get(y,i);
		double dyi = gsl_vector_get(dy,i);
	/*	Set b_i = y_i / dy_i	*/
		gsl_vector_set(b,i,yi/dyi);
		for(int j = 0; j <m; j++){ 		/* column */
		/*	Set A_ik = f_k(x_i)/dy_i	*/
			gsl_matrix_set(A,i,j,func(j,xi)/dyi);
		}
	}

/*	QR operations	*/
/*	Decompose A to QR, destroying A */
	gs_decomp(A,Q,R);

/*	Solve R c = Q^T b, returning the vector of the coefficients, c 	*/
	gs_solve(Q,R,b,c);

/*	Calculate covariance matrix invR * invR^T 	*/
/*	get invR from R 	*/
	gs_inverse(I,R,invR);
/*	Calculate covariance	*/
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,invR,invR,0,covariance);

	for (int i = 0; i<m; i++){
		gsl_vector_set(c_error,i,
			sqrt(gsl_matrix_get(covariance,i,i)));
	}

	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_matrix_free(invR);
	gsl_matrix_free(I);
}