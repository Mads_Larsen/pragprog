#include <stdio.h>
#include <limits.h>
#include <float.h>
	
void precission(){
	// 1
	printf("1.\n");

	// 1.i maximum integer
	printf("1.i Max int for different loops. Notice that they return same max int\n");
	printf("limits.h INT_MAX = 	%i\n",INT_MAX);
	// While loop
	int i = 1;
	while (i+1>i){i++;}
		
	printf("While loop max int =	%i\n", i );

	// For loop
	i = 1;
	for (i;i+1>i;i++);
	
	printf("For loop max int =	%i\n", i);

	// Do While loop
	i=1;
	do
		i++;
		while(i+1>i);
	printf("Do while loop max int =	%i\n",i );


	// 1.ii Minimum integer.
	printf("\n1.ii Minimum integer\n");
	printf("Minimum integer INT_MAX =	%i\n", INT_MIN);
	i = 1;
	while (i-1<i){i--;}
	printf("For loop min int =		%i\n", i);

	// 1.iii Machine epsilon
	printf("\n1.iii The Machine Epsilon\n");
	float xf = 1.0f; double xd = 1.0; long double xld = 1.0L;

	//While loop
	printf("while loop\n");
	while(1+xf!=1){xf/=2.0;} 
	xf*=2.0;
	while(1+xd!=1){xd/=2.0;} 
	xd*=2.0;
	while(1+xld!=1){xld/=2.0L;} 
	xld*=2.0L;

	printf("FLT_EPSILON =	%g\nFloat =		%g\n", FLT_EPSILON, xf);
	printf("DBL_EPSILON =	%g\nDouble =	%g\n", DBL_EPSILON, xd);
	printf("LDBL_EPSILON =	%Lg\nLong Double =	%Lg\n", LDBL_EPSILON, xld);

	//for loop
	printf("\nFor loop\n");
	for(xf; 1+xf!=1; xf/=2.0); 
	xf*=2.0;
	for(xd; 1+xd!=1; xd/=2.0); 
	xd*=2.0;
	for(xld; 1+xld!=1; xld/=2.0); 
	xld*=2.0L;

	printf("FLT_EPSILON =	%g\nFloat =		%g\n", FLT_EPSILON, xf);
	printf("DBL_EPSILON =	%g\nDouble =	%g\n", DBL_EPSILON, xd);
	printf("LDBL_EPSILON =	%Lg\nLong Double =	%Lg\n", LDBL_EPSILON, xld);

	//Do While loop
	printf("\nDo while loop\n");
	do
		xf/=2.0;
		while(1+xf!=1);
		xf*=2.0;
	do
		xd/=2.0;
		while(1+xd!=1);
		xd*=2.0;
	do
		xld/=2.0L;
		while(1+xld!=1);
		xld*=2.0L;

	printf("FLT_EPSILON =	%g\nFloat =		%g\n", FLT_EPSILON, xf);
	printf("DBL_EPSILON =	%g\nDouble =	%g\n", DBL_EPSILON, xd);
	printf("LDBL_EPSILON =	%Lg\nLong Double =	%Lg\n", LDBL_EPSILON, xld);

	return 0;
}