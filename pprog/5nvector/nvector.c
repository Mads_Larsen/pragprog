#include <stdio.h>
#include <stdlib.h>
#include "nvector.h"
#include <tgmath.h>
#define TINY 10.0

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);}

void nvector_set(nvector* v, int i, double value){ (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

/* ... */

void nvector_print    (char* s, nvector* v){    /* prints s and then vector */
	printf("%s ",s);
	for(int i = 0; i < (*v).size; i++){
		printf("%g ", (*v).data[i]);
	}
	printf("\n");
}


void nvector_add      (nvector* a, nvector* b){ /* a_i ← a_i + b_i */
	for(int i=0; i<(*a).size; i++){
		(*a).data[i] += (*b).data[i];

	}
	return a;
}

double   nvector_dot_product (nvector* u, nvector* v){   /* returns dot-product */
	double d=0;
	for (int i=0; i<(*u).size; i++){
		d += (*u).data[i] * (*v).data[i];
	}
	return d;
}

void nvector_set_zero (nvector* v){            /* all elements ← 0 */
	for(int i=0; i < (*v).size; i++){
		(*v).data[i] = 0;
	}
}

// TESTS.  CHECKS IF RESULTS ARE CORRECT

int  nvector_equal    (nvector* a, nvector* b){ /* 1, if equal, 0 otherwise */
	if(a == b)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE;
}

int double_equal (double a, double b){ // Virker ikke. Kode fra komplex.
	double TAU = TINY; double EPS = TINY; // Resten er ens
	if (fabs(a-b)< TAU || fabs(a-b)<EPS* (fabs(a) + fabs(b)))
//	if (a==b)
		return EXIT_SUCCESS;
	else
		return EXIT_FAILURE; // Læsbar måde at skrive 0/1
}