#include "header.h"

int main(){
	fprintf(stderr, "\n____________________________________________________\n");
	fprintf(stderr, "\n C. Clenshaw-Curtis Variable Transformation\n");
	fprintf(stderr, "____________________________________________________\n");


	int evals = 0;
	double Q, a, b, err, acc=0.000001, eps=0.000001;
	double func_exp (double x){
		double r= exp(-pow(x,2));
		return r;
	};				/* = 2/3	*/

	fprintf(stderr, "Integral of exp(-x²)\n");
	fprintf(stderr, "limit\t calculated\t exact\t evals\t error\n");

	a = -1; b = 1;	
	Q = integrator_Clenshaw_Curtis(func_exp,a,b,acc,eps,&err);
	fprintf(stderr, "-1,1\t %g\t 1.49365\t %i\t %g\n", Q, evals, err);

	return EXIT_SUCCESS;
}