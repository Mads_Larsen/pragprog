#include "header.h"

int newton(
double f(gsl_vector* p, gsl_vector* gradient),/* f: objective function, gradient: gradient, H: Hessian matrix H*/
void hessian(gsl_vector * p, gsl_matrix * H),
gsl_vector* x, /* starting point, becomes the latest approximation to the root on exit */
double accuracy) /* accuracy goal, on exit |gradient|<eps */
{
	/*	Allocate Memory	*/
	int n=x->size;
	gsl_matrix* H = gsl_matrix_alloc(n,n);
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* gradient = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);

	int  steps=0;
/*	Iterate untill satisfactory minimum x are found	*/
	do{
		f(x,gradient);
		hessian(x,H);
	/* 	Gram schmidt solve Newtons step,  Delta x
		H DeltaX = -gradient	*/
		gs_decomp(H,Q,R);
		gs_solve(Q,R,gradient,Dx);
		gsl_vector_scale(Dx,-1);

		backtracking_linesearch(f,x,Dx); /* See Tools.c */
		f(x,gradient);
		steps++;
	} while(gsl_blas_dnrm2(gradient)>accuracy && steps < 1000);
/*	Convergence criterion is ‖gradient(x)/dx‖<ε	*/
	if(steps == 1000)fprintf(stderr, "Did not converge\n");

/*	Free Allocated Memory	*/
	gsl_matrix_free(H);		gsl_matrix_free(Q);	gsl_matrix_free(R);
	gsl_vector_free(gradient);	gsl_vector_free(Dx);

	return steps;
}

int quasi_newton(
double f(gsl_vector* p, gsl_vector* gradient),/* f: objective function, gradient: gradient, H: Hessian matrix H*/
gsl_vector* x, /* starting point, becomes the latest approximation to the root on exit */
double accuracy) /* accuracy goal, on exit |gradient|<eps */
{
	/*	Allocate Memory	*/
	int n=x->size;

	gsl_matrix *H = gsl_matrix_alloc(n,n);
	gsl_vector *grad_fx = gsl_vector_alloc(n);
	gsl_vector *y = gsl_vector_alloc(n);
	gsl_vector *s = gsl_vector_alloc(n);
	gsl_vector *Hy = gsl_vector_alloc(n);

	double Hys;
	int  steps=0;
	int  step_max = 100000;

/*	One usually starts with a unity matrix as the zeroth approximation 
	for the inverse Hessian matrix 	*/
	gsl_matrix_set_identity(H);
	f(x,grad_fx);

/*	Iterate untill satisfactory minimum x are found	*/
	do{
	/*	Solve  ∇f(x + s) = ∇f(x) + Hs, setting gradient to zero (find min) we get 
		s = -H grad fx 		Where s is a step size*/
		gsl_blas_dgemv(CblasNoTrans,-1.0,H,grad_fx,0.0,s);
	/*	Find new x 	*/
		backtracking_linesearch_qn(f,H,x,s);

	/* 	Solve Broydens Update ! 
		H^-1 → H^−1 + (s−H^−1y)s^T H^−1 /  y^TH^−1 s 	*/
	/*	find y = f(x+s)-f(x)	*/
		f(x,y); /* y <- f(x+s) for memory saving */
		gsl_vector_sub(y,grad_fx);

		gsl_blas_dgemv(CblasNoTrans,1.0,H,y,0.0,Hy); /* H*y */
		gsl_blas_dgemv(CblasNoTrans,1.0,H,s,0.0,grad_fx); /* ∇f(x) <- Hs for memory saving */
		gsl_blas_ddot(Hy,s,&Hys); /* y H s  to be used as 1/yHs	*/
		gsl_vector_sub(s,Hy); /* s <- s-Hy */

		if(fabs(Hys)>accuracy){
	/* 	Broyden Update - Rank 1 update  A = a x y^T + A*/
	/*				(1/Hys)	*(s-Hy) * Hs  	+ H 	*/
		gsl_blas_dger(1/Hys, 	s,	grad_fx, H);
		}
		f(x,grad_fx); /* Redefine ∇f(x)	*/
		steps++;
	} while(gsl_blas_dnrm2(grad_fx)>accuracy && steps < step_max);
/*	Convergence criterion is ‖gradient(x)/dx‖<ε	*/
	if(steps == step_max)fprintf(stderr, "Did not converge\n");

/*	Free Allocated Memory	*/
	gsl_matrix_free(H);
	gsl_vector_free(grad_fx);
	gsl_vector_free(y);
	gsl_vector_free(s);
	gsl_vector_free(Hy);

	return steps;
}
