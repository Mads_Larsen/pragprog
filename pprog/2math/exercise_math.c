#include <stdio.h>
#include <complex.h>
#include <tgmath.h> /* Mere generel end 'math'. tilpasser sig input typen*/

int main(){

	// Find gamma function of 5.0
	double x = tgamma(5.0);
	printf("The gamma function of 5.0 is %g\n", x);

	// Find first order bessel function of 0.5
	double y = j1(0.5);
	printf("Bessel func. of 1st kind J1(0.5) = %g\n",y);

	// Find complex expressions
	double complex z[4] = {csqrt(-2.0), exp(I), exp(I*M_PI), pow(I,M_E)};
	printf("Here's an array of complex functions\n");

	int i;
	for (i=0; i<4; i++){
		printf("%g  %+gi\n", creal(z[i]), cimag(z[i]) );
	}
	
	// How many digits can variable types store?
	float fa = 0.1111111111111111111111111111f;
	double da = 0.1111111111111111111111111111;
	long double lda = 0.1111111111111111111111111111L;
	
	printf("Digits stored in:\nfloat:		%.25g\n"
		"double:		%.25lg\n"
		"long double:	%.25Lg\n",
	 	fa, da,lda);

	return 0;
}
