#include "header.h"

int main(int argc, char const *argv[]){
	int n = 6; /* Dimension */
/*	If arguments are passed to the function, change n */
	if(argc > 1){
		n = atoi(argv[1]);
	}

	fprintf(stderr, "\n A. Jacobi diagonalization with cyclic sweeps\n");

/*	Square symmetric matrix */
	gsl_matrix * A = gsl_matrix_alloc(n,n);
	gsl_matrix * eigvec = gsl_matrix_alloc(n,n);
	gsl_vector * eigval = gsl_vector_alloc(n);

/*	Fill A matrix and copy it */
	matrix_fill_sym(A);
//	gsl_matrix_memcpy(A_copy,A);
	matrix_print("Symmetrix matrix A", A);

	int sweeps = jacobian_sweep(A, eigvec, eigval, 0);
	fprintf(stderr, "\nsweeps = %i\n", sweeps);
	matrix_print("Eigenvectors", eigvec);
	fprintf(stderr, "\nEigenvalues\n");
	gsl_vector_fprintf(stderr, eigval, "%g");
	matrix_rebuild_sym(A);

/*	Check if V^T A V = D */
	gsl_matrix* AV = gsl_matrix_calloc(n, n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, eigvec, 0, AV);
	gsl_matrix *VTAV = gsl_matrix_calloc(n, n);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, eigvec, AV, 0, VTAV);
	matrix_print("Matrix V^T A V = should be eigenvalues", VTAV);

/*	Free allocated memory	*/
	gsl_matrix_free(A);	
	gsl_matrix_free(eigvec); gsl_vector_free(eigval);
	
	return EXIT_SUCCESS;
}